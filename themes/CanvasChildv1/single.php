<?php
/**
 * Single Post Template
 *
 * This template is the default page template. It is used to display content when someone is viewing a
 * singular view of a post ('post' post_type).
 * @link http://codex.wordpress.org/Post_Types#Post
 *
 * @package WooFramework
 * @subpackage Template
 */

get_header();
?>
       
    <!-- #content Starts -->
	<?php woo_content_before(); ?>
    <div id="content" class="col-full">
    
    	<div id="main-sidebar-container">    

            <!-- #main Starts -->
            <?php woo_main_before(); ?>
            <section id="main">                       
<?php
	woo_loop_before();
	
	if (have_posts()) { $count = 0;
		while (have_posts()) { the_post(); $count++;
			
			woo_get_template_part( 'content', get_post_type() ); // Get the post content template file, contextually.
		}
	}
	
	woo_loop_after();
?>     


<!-- BeginRelatedPosts -->

<?php 
			
			/////
			// RELATED POSTS, Option 2: all pages in current post's category
			/////
			
			//Ideas for future:
			/* 
				Take the title, remove stop words (articles, etc.), and search all posts by remaining words and display first set
				Track views to pages using a custom field, then pull all posts and sort by the custom field and return top set (writing to DB for every page may slow things down alot, better if DB can track views itself)
				Add featured images to use instead of solid color boxes
			*/
			

			$categories=get_the_category();
			//print_r($categories);
			//echo "<br />Category Name: ".$categories[0]->term_id."<br />";

			if($categories[0]->term_id!="") : ?>

				<?php 
					//The following are the default variables for get_posts in case we want to change them
					$args = array(
					'category'         => $categories[0]->term_id,
					'posts_per_page'   => 9,
					'offset'		   => 1,
					'orderby'          => 'post_date',
					'order'            => 'DESC',
					'include'          => '',
					'exclude'          => '',
					'meta_key'         => '',
					'meta_value'       => '',
					'post_type'        => array( 'post', 'page' ),
					'post_mime_type'   => '',
					'post_parent'      => '',
					'post_status'      => 'publish',
					'suppress_filters' => true ); ?>
				<?php $posts_array = get_posts( $args ); 
				//posts per page limits how many are returned at bottom of page
				//offset says that if there is only one in the list, then don't return anything anyway
				//echo count($posts_array);  //removed since post_array is empty if only one is returned
				//print_r ($posts_array);

				if( $posts_array ) : ?>
					<div id="frn_related_posts">
						<a name="further_reading"></a><h3>Related Posts to <?=$categories[0]->name; ?></h3>
						<div class="rp_list">
						<ul>

						<?php 
						$count=1;
						foreach ( $posts_array as $post_new ) : setup_postdata( $post_new );
							//skips current post in list
							if( $post_new->ID != get_the_ID() && $count<9) {
								$colors = array("68B3AF", "87BDB1", "AACCB1");
								$color = array_rand($colors, 1);
								$color_hover_styles = array("color_hvr_grn", "color_hvr_bl", "color_hvr_org", "color_hvr_rd", "color_hvr_ppl");  //grays: array("C3DBB4", "D3E2B6", "A8DBA8");
								$color_hover_style = array_rand($color_hover_styles, 1);
								//echo $color;
								//echo $count;
						?>
							<a href="<?php echo get_permalink($post_new->ID); ?>">
								<li class="<?=$color_hover_styles[$color_hover_style]; ?>" style="background-color:#<?=$colors[$color]; ?>;">
									<div class="positioning"><div class="overflow_limit"><?php 
										echo trim(substr($post_new->post_title, 0, 52)); if(strlen($post_new->post_title)>52) echo "…"; 
										?></div></div>
								</li>
							</a>
							<?php
							$count++;
							}
							else $count=$count-1;
						endforeach;
						?>
						</ul>
						</div>
						<div style="clear:both;"></div>
						<div class="frn_rp_more"><h4><a href="<?=home_url();?>/category/<?=$categories[0]->slug; ?>/">See All Related &gt;</a></h4></div>
					</div>
					<?php
				endif;
				wp_reset_postdata();

				//print_r($posts_array);

			endif;
			?> 
			
		
		
<!-- EndRelatedPosts -->

          
</section>
<!-- /#main -->
            <?php woo_main_after(); ?>
    
            <?php get_sidebar(); ?>

		</div><!-- /#main-sidebar-container -->         

		<?php get_sidebar('alt'); ?>

    </div><!-- /#content -->
	<?php woo_content_after(); ?>

<?php get_footer(); ?>