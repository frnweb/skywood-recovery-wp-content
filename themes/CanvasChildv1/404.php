<?php
/**
 * The template for displaying 404 pages (Not Found)
 *
 * @package WordPress
 * @subpackage Opiate Child
 * 
 */

 get_header();
 global $woo_options, $query_context;
	unset( $query_context );
	$query_context = new stdClass();
	$query_context->context = array();
	$query_context->context[] = 'search';

//example url causing 404: http://www.opiaterehabtreatment.com/v2/are-there-homeopathic-treatments-for-opiates2
//get url
//$wp_query->query_vars['name'];
//remove root install address
//change dashes to spaces
//remove articles and useless words
//that becomes the search

//$s = str_replace(get_site_url(),"",$wp_query->query_vars['name']);
$s = $wp_query->query_vars['name'];
$s = trim(preg_replace("/(.*)-(html|htm|php|asp|aspx)$/","$1",$s));
$posts = query_posts( array( 'post_type' => 'any', 'name' => $s) );
//if no single page is found, then turn it into a search
if (count($posts) == 0) {
	$s = trim(str_replace("-"," ",$s));
	$s = urldecode($s);
	$s = strtolower(preg_replace('/[0-9]+/', '', $s )); //remove all numbers
	$stop_words = array(
		"for",
		"the",
		"and",
		"an",
		"a",
		"is",
		"are",
		"than",
		"that",
		"I",
		"to",
		"on",
		"it",
		"with",
		"can",
		"be",
		"of",
		"get",
		"in",
		"you",
		"from",
		"if",
		"by",
		"so",
		"at",
		"do",
		"&",
		"there",
		"too"
	);
	$i=1;
	foreach($stop_words as $word){
		/*
		if($i==1) {
			echo $s."<br />";
			echo $word."<br />";
			echo "string: ".strlen($s);
			echo "; word: ".strlen(" ".$word)."<br />";
			echo "position: ".strpos($s,$word." ")."<br />";
			echo "string without word: ".(strlen($s)-strlen(" ".$word))."<br />";
		}
		*/
		$word = trim(strtolower($word));
		$s = str_replace(" ".$word." "," ",$s); ///in the middle
		if(strpos($s,$word." ")===0) $s = str_replace($word." ","",$s); // at the beginning
		if(strpos($s," ".$word)==strlen($s)-strlen(" ".$word)) $s = str_replace(" ".$word,"",$s); // at the end
		$i++;
	}
	//echo $s."<br />";
	$search_query['s'] = $s;
	$search_query['submit'] = "Search";
	$the_query = new WP_Query($search_query);
	//$posts = query_posts(array( array('post_type' => 'any', 'name' => $s) ));
}

?>      
    
    <!-- #content Starts -->
	<?php woo_content_before(); ?>
	
    <div id="content" class="col-full">
    
    	<div id="main-sidebar-container">    
		
            <!-- #main Starts -->
            <?php woo_main_before(); ?>
            <section id="main" class="col-left">
				<h2><?php _e( 'Whoops!', 'opiatechild' ); ?></h2>
				<br />
				<p><?php _e( 'It looks like something may be wrong with the web address you were using.', 'opiatechild' ); ?></p>
				<br />
			<?php
               
                if ( $the_query->have_posts() ) { ?>
				<p><b>Were you looking for:</b></p>
				<br />
				<ol class="frn_suggestions">
				<?php
				$i=1;
				while ( $the_query->have_posts() && $i<=8) {
					$the_query->the_post(); 
					if(get_the_id()!=get_option('page_on_front')) { 
					?>
					<li>
						<a href="<?php the_permalink();?>"><?php the_title();?></a>
					</li>
					<?php 
					}
					$i++;
				}
				?>
				</ol>
				<br />
				<br />
				<?php
				} else { ?>
					<p>
						And unfortunately, we can't find any posts that relate to that web address. 
						Take a look at it in the address bar above and see if it looks pretty normal. 
						Make corrections if not and try again.
					</p>
				<?php }
                
            ?>

				<br />
				<p><b><?php _e( '...or maybe try searching:', 'opiatechild' ); ?></b></p>
                <div class="fl" style="margin-bottom:100px;"><?php get_search_form(); ?></div>
            </section><!-- /#main -->
            <?php woo_main_after(); ?>
    
            <?php get_sidebar(); ?>
    
		</div><!-- /#main-sidebar-container -->         

		<?php get_sidebar( 'alt' ); ?>       

    </div><!-- /#content -->
	<?php woo_content_after(); 
	
	/* Restore original Post Data */
	wp_reset_postdata();
	?>
		
<?php get_footer(); ?>