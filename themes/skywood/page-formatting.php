<?php
/*
Template Name: Formatting
*/
?>

<?php get_header(); ?>
			
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			<?php 
				$post_thumbnail_id = get_post_thumbnail_id();
				$featured_src = wp_get_attachment_image_src( $post_thumbnail_id, 'full' );
				$featured_mobile_src = get_field( 'mobile_featured_image');
			?>
<style>
code {
    background: none;
    border: none;
}
</style>

<div class="row">
<div class="large-12 columns">
<div id="content">
<div id="left-content-block">
	
<div class="row">
	<div class="large-6 columns">
	<h3><small>Rendered</small></h3>
		<h1>h1. This is a very large header.</h1>
		<h2>h2. This is a large header.</h2>
		<h3>h3. This is a medium header.</h3>
		<h4>h4. This is a moderate header.</h4>
		<h5>h5. This is a small header.</h5>
		<h6>h6. This is a tiny header.</h6>
	</div>
	<div class="large-6 columns">
	<h3><small>HTML</small></h3>
	<code><textarea style="height:140px;">
<h1>h1. This is a very large header.</h1>
<h2>h2. This is a large header.</h2>
<h3>h3. This is a medium header.</h3>
<h4>h4. This is a moderate header.</h4>
<h5>h5. This is a small header.</h5>
<h6>h6. This is a tiny header.</h6>
	</textarea></code>
	</div>
</div>

<h2>Subheaders</h2>
<div class="row">
	<div class="large-6 columns">
	<h3><small>Rendered</small></h3>
		<h1 class="subheader">h1.subheader</h1>
		<h2 class="subheader">h2.subheader</h2>
		<h3 class="subheader">h3.subheader</h3>
		<h4 class="subheader">h4.subheader</h4>
		<h5 class="subheader">h5.subheader</h5>
		<h6 class="subheader">h6.subheader</h6>
	</div>
	<div class="large-6 columns">
	<h3><small>HTML</small></h3>
	<code><textarea style="height:140px;">
<h1 class="subheader">h1.subheader</h1>
<h2 class="subheader">h2.subheader</h2>
<h3 class="subheader">h3.subheader</h3>
<h4 class="subheader">h4.subheader</h4>
<h5 class="subheader">h5.subheader</h5>
<h6 class="subheader">h6.subheader</h6>
	</textarea></code>
	</div>
</div>

<h2>Small header segments</h2>
<div class="row">
	<div class="large-6 columns">
	<h3><small>Rendered</small></h3>
		<h1>h1. <small>Small segment header.</small></h1>
		<h2>h2. <small>Small segment header.</small></h2>
		<h3>h3. <small>Small segment header.</small></h3>
		<h4>h4. <small>Small segment header.</small></h4>
		<h5>h5. <small>Small segment header.</small></h5>
		<h6>h6. <small>Small segment header.</small></h6>
	</div>
	<div class="large-6 columns">
	<h3><small>HTML</small></h3>
	<code><textarea style="height:140px;">
<h1>h1. <small>Small segment header.</small></h1>
<h2>h2. <small>Small segment header.</small></h2>
<h3>h3. <small>Small segment header.</small></h3>
<h4>h4. <small>Small segment header.</small></h4>
<h5>h5. <small>Small segment header.</small></h5>
<h6>h6. <small>Small segment header.</small></h6>
	</textarea></code>
	</div>
</div>
<hr>

<h2>Paragraph Classes</h2>
<div class="row">
	<div class="large-6 columns">
	<h3><small>Large lead paragraphs</small></h3>
<p class="lead">Today, treatment for addiction can take many forms and incorporate several elements, including residential and outpatient programs drug or alcohol detox, co-occurring treatment, step-down programs, sober living and aftercare. Addiction treatment has come a long way, with targeted research providing clear direction on which models are the most effective for an individual’s specific needs and pointing to new approaches that deliver better results than the programs of years past.</p>
	</div>
	<div class="large-6 columns">
	<h3><small>HTML</small></h3>
	<code><textarea style="height:140px;">
<p class="lead">Today, treatment for addiction can take many forms and incorporate several elements, including residential and outpatient programs drug or alcohol detox, co-occurring treatment, step-down programs, sober living and aftercare. Addiction treatment has come a long way, with targeted research providing clear direction on which models are the most effective for an individual’s specific needs and pointing to new approaches that deliver better results than the programs of years past.</p>
	</textarea></code>
	</div>
	</div>

<div class="row">
	<div class="large-6 columns">
			<h3><small>Regular paragraph</small></h3>
<p>Today, treatment for addiction can take many forms and incorporate several elements, including residential and outpatient programs drug or alcohol detox, co-occurring treatment, step-down programs, sober living and aftercare. Addiction treatment has come a long way, with targeted research providing clear direction on which models are the most effective for an individual’s specific needs and pointing to new approaches that deliver better results than the programs of years past.</p>
	</div>
	<div class="large-6 columns">
	<h3><small>HTML</small></h3>
	<code><textarea style="height:140px;">
<p>Today, treatment for addiction can take many forms and incorporate several elements, including residential and outpatient programs drug or alcohol detox, co-occurring treatment, step-down programs, sober living and aftercare. Addiction treatment has come a long way, with targeted research providing clear direction on which models are the most effective for an individual’s specific needs and pointing to new approaches that deliver better results than the programs of years past.</p>
	</textarea></code>
	</div>
</div>

<hr>
<h2>Text Align Classes</h2>
<div class="row">
	<div class="large-6 columns">
	<h3><small>Basic</small></h3>
		<p class="text-left"><strong>Left Aligned.</strong> Set in the year 0 F.E. ("Foundation Era"), The Psychohistorians opens on Trantor, the capital of the 12,000-year-old Galactic Empire.</p>
		<p class="text-right"><strong>Right Aligned.</strong> Set in the year 0 F.E. ("Foundation Era"), The Psychohistorians opens on Trantor, the capital of the 12,000-year-old Galactic Empire.</p>
		<p class="text-center"><strong>Center Aligned.</strong> Set in the year 0 F.E. ("Foundation Era"), The Psychohistorians opens on Trantor, the capital of the 12,000-year-old Galactic Empire.</p>
		<p class="text-justify"><strong>Justified.</strong> Set in the year 0 F.E. ("Foundation Era"), The Psychohistorians opens on Trantor, the capital of the 12,000-year-old Galactic Empire.</p>
	</div>
	<div class="large-6 columns">
	<h3><small>HTML</small></h3>
	<code><textarea style="height:140px;">
<p class="text-left"><!-- text goes here --></p>
<p class="text-right"><!-- text goes here --></p>
<p class="text-center"><!-- text goes here --></p>
<p class="text-justify"><!-- text goes here --></p>
	</textarea></code>
	</div>
</div>
<hr>
<h2>Lists</h2>
<div class="row">
	<div class="large-6 columns">
	<h3><small>Basic</small></h3>
	<ul>
	  <li>List item with a much longer description or more content.</li>
	  <li>List item</li>
	  <li>List item
	    <ul>
	      <li>Nested List Item</li>
	      <li>Nested List Item</li>
	      <li>Nested List Item</li>
	    </ul>
	  </li>
	  <li>List item</li>
	  <li>List item</li>
	  <li>List item</li>
	</ul>
	</div>
	<div class="large-6 columns">
	<h3><small>HTML</small></h3>
	<code><textarea style="height:140px;">
<ul>
<li>List item with a much longer description or more content.</li>
<li>List item</li>
<li>List item
<ul>
<li>Nested List Item</li>
<li>Nested List Item</li>
<li>Nested List Item</li>
</ul>
</li>
<li>List item</li>
<li>List item</li>
<li>List item</li>
</ul>
	</textarea></code>
	</div>
</div>


<div class="row">
	<div class="large-6 columns">
	<h3><small>No Bullet</small></h3>
	<ul class="no-bullet">
	  <li>List item with a much longer description or more content.</li>
	  <li>List item</li>
	  <li>List item
	    <ul>
	      <li>Nested List Item (has bullets)</li>
	      <li>Nested List Item (has bullets)</li>
	      <li>Nested List Item (has bullets)</li>
	    </ul>
	  </li>
	  <li>List item</li>
	  <li>List item</li>
	  <li>List item</li>
	</ul>
	</div>
	<div class="large-6 columns">
	<h3><small>HTML</small></h3>
	<code><textarea style="height:140px;">
<ul class="no-bullet">
<li>List item with a much longer description or more content.</li>
<li>List item</li>
<li>List item
<ul>
<li>Nested List Item</li>
<li>Nested List Item</li>
<li>Nested List Item</li>
</ul>
</li>
<li>List item</li>
<li>List item</li>
<li>List item</li>
</ul>
	</textarea></code>
	</div>
</div>
<div class="row">
	<div class="large-6 columns">
	<h3><small>Ordered/Numbered List</small></h3>
	<ol>
	  <li>List item with a much longer description or more content.</li>
	  <li>List item</li>
	  <li>List item
	    <ol>
	      <li>Nested List Item</li>
	      <li>Nested List Item</li>
	      <li>Nested List Item</li>
	    </ol>
	  </li>
	  <li>List item</li>
	  <li>List item</li>
	  <li>List item</li>
	 </ol>
	</div>
	<div class="large-6 columns">
	<h3><small>HTML</small></h3>
	<code><textarea style="height:140px;">
<ol>
<li>List item with a much longer description or more content.</li>
<li>List item</li>
<li>List item
<ol>
<li>Nested List Item</li>
<li>Nested List Item</li>
<li>Nested List Item</li>
</ol>
</li>
<li>List item</li>
<li>List item</li>
<li>List item</li>
</ol>
	</textarea></code>
	</div>
</div>
<hr>
<h2>Blockquotes</h2>
<div class="row">
		<div class="large-6 columns">
		<h3>Regular Blockquote</h3>
			<blockquote>Those people who think they know everything are a great annoyance to those of us who do.<cite>Isaac Asimov</cite></blockquote>			
		</div>
		<div class="large-6 columns">
			<h3><small>HTML</small></h3>	
			<code><textarea style="height:140px;">
<blockquote>Those people who think they know everything are a great annoyance to those of us who do.<cite>Isaac Asimov</cite></blockquote>
</textarea></code>

		</div>
</div>

<div class="row">
	<div class="large-6 columns">
		<h3>Large Blockquote</h3>
			<div class="quote">
			<blockquote>Those people who think they know everything are a great annoyance to those of us who do.<cite>Isaac Asimov</cite></blockquote>
			</div>
	</div>
	<div class="large-6 columns">
		<h3><small>HTML</small></h3>
<code><textarea style="height:140px;">
<div class="quote">
<blockquote>Those people who think they know everything are a great annoyance to those of us who do.<cite>Isaac Asimov</cite></blockquote>
</div>	
			</textarea></code>
	</div>
</div>

<hr>
<a name="btns"></a>
<h2 data-magellan-destination="btns">Buttons</h2>
	<div class="row">
		<div class="large-6 columns">
			<h3><small>Default</small></h3>
			<a href="#" class="button">Button</a>
			<h3><small>Orange</small></h3>
			<a href="#" class="button secondary">Orange Button</a>
		</div>
		<div class="large-6 columns">
			<h3><small>HTML</small></h3>	
			<code><textarea style="height:140px;">
<a href="#" class="button">Button</a>
<a href="#" class="button secondary">Orange Button</a>
			</textarea></code>
		</div>
	</div>

	<div class="row">
		<div class="large-6 columns">
			<h3><small>Sizes</small></h3>
			<a href="#" class="button tiny">Tiny Button</a><br />
			<a href="#" class="button small">Small Button</a><br />
			<a href="#" class="button large">Large Button</a><br />
		</div>
		<div class="large-6 columns">
			<h3><small>HTML</small></h3>	
			<code><textarea style="height:140px;">
<a href="#" class="button tiny">Tiny Button</a>
<a href="#" class="button small">Small Button</a>
<a href="#" class="button large">Large Button</a>
			</textarea></code>
		</div>
	</div>
			
<div class="row">
		<div class="large-6 columns">
			<h3><small>Floats</small></h3>
			<a href="#" class="button float-left">Left Float</a>
			<a href="#" class="button float-right">Right Float</a>
		</div>
		<div class="large-6 columns">
			<h3><small>HTML</small></h3>	
			<code><textarea style="height:140px;">
<a href="#" class="button float-left">Left Float</a>
<a href="#" class="button float-right">Right Float</a>
			</textarea></code>
		</div>
</div>

<div class="row">
		<div class="large-6 columns">
			<h3><small>Centered</small></h3>
			<div class="text-center">
				<a href="#" class="button float-center">Centered</a>
			</div>
		</div>
		<div class="large-6 columns">
			<h3><small>HTML</small></h3>	
			<code><textarea style="height:140px;">
<div class="text-center">
	<a href="#" class="button float-center">Centered</a>
</div>
			</textarea></code>
		</div>
</div>

<hr>
<h2>Labels</h2>
<p>
	Labels are useful inline styles that can be dropped into body copy to call out certain sections or to attach metadata. For example, you can attach a label that notes when something was updated.
</p>
<h3>Basic</h3>
<p>
	You can create a label using minimal markup.
</p>
<div class="row">
		<div class="large-6 columns">
			<h3><small>Default</small></h3>
			<span class="label">Regular Label</span>				
		</div>
		<div class="large-6 columns">
			<h3><small>HTML</small></h3>	
			<code><textarea style="height:140px;">
<span class="label">Regular Label</span>				
			</textarea></code>
		</div>
</div>
<h3>Advanced</h3>
<p>
	Additional classes can be added to your labels to change their appearance.
</p>
<div class="row">
		<div class="large-6 columns">
			<h3><small>Default</small></h3>
			<p>
				<span class="success round label">Success Round</span><br>		
				<span class="alert radius label">Alert Radius</span><br>
				<span class="secondary label">Secondary Label</span></p>
		</div>
		<div class="large-6 columns">
			<h3><small>HTML</small></h3>	
			<code><textarea style="height:140px;">
<span class="success round label">Success Round</span>				
<span class="alert radius label">Alert Radius</span>
<span class="secondary label">Secondary Label</span>				
			</textarea></code>
		</div>
</div>
<br>
<hr>
<h2>Visibility</h2>
<p>
	Visibility classes let you show or hide elements based on screen size. You can use visibility classes to control which elements users see depending on their browsing environment.
</p>
<h3>Show by Screen Size</h3>
<p>
	In this example, we use the show visibility classes to show certain strings of text based on the device on which users view a page. If their browser meets the class's conditions, the element will be shown. If not, it will be hidden.
</p>
<div class="row">
		<div class="large-6 columns">
			<h3><small>Default</small></h3>
			<p class="panel">
			  <strong class="show-for-small-only">This text is shown only on a small screen.</strong>
			  <strong class="show-for-medium-up">This text is shown on medium screens and up.</strong>
			  <strong class="show-for-medium-only">This text is shown only on a medium screen.</strong>
			  <strong class="show-for-large-up">This text is shown on large screens and up.</strong>
			  <strong class="show-for-large-only">This text is shown only on a large screen.</strong>			  
			</p>				
		</div>
		<div class="large-6 columns">
			<h3><small>HTML</small></h3>	
			<code><textarea style="height:140px;">
<p>
  <strong class="show-for-small-only">This text is shown only on a small screen.</strong>
  <strong class="show-for-medium-up">This text is shown on medium screens and up.</strong>
  <strong class="show-for-medium-only">This text is shown only on a medium screen.</strong>
  <strong class="show-for-large-up">This text is shown on large screens and up.</strong>
  <strong class="show-for-large-only">This text is shown only on a large screen.</strong>
</p>
			</textarea></code>
		</div>
</div>
<br>
<h3>Hide by Screen Size</h3>
<p>
	This example shows the opposite: It uses the hide visibility classes to state which elements should disappear based on your device's screen size or orientation. Users will see elements on every browser except those that meet these conditions.
</p>
<div class="row">
		<div class="large-6 columns">
			<h3><small>Default</small></h3>
			<p class="panel">
			  <strong class="hide-for-small-only">You are <em>not</em> on a small screen.</strong>
			  <strong class="hide-for-medium-up">You are <em>not</em> on a medium, large, xlarge, or xxlarge screen.</strong>
			  <strong class="hide-for-medium-only">You are <em>not</em> on a medium screen.</strong>
			  <strong class="hide-for-large-up">You are <em>not</em> on a large, xlarge, or xxlarge screen.</strong>
			  <strong class="hide-for-large-only">You are <em>not</em> on a large screen.</strong>
			</p>
		</div>
		<div class="large-6 columns">
			<h3><small>HTML</small></h3>	
			<code><textarea style="height:140px;">
<p>
  <strong class="hide-for-small-only">You are <em>not</em> on a small screen.</strong>
  <strong class="hide-for-medium-up">You are <em>not</em> on a medium, large, xlarge, or xxlarge screen.</strong>
  <strong class="hide-for-medium-only">You are <em>not</em> on a medium screen.</strong>
  <strong class="hide-for-large-up">You are <em>not</em> on a large, xlarge, or xxlarge screen.</strong>
  <strong class="hide-for-large-only">You are <em>not</em> on a large screen.</strong>
</p>		</textarea></code>
		</div>
</div>
<hr>
<a name="structure"></a>
<h2 data-magellan-destination="structure">Grid System</h2>	
<p>
	Start by adding an element with a class of row. This will create a horizontal block to contain vertical columns, 12 total. Then add divs with a column class within that row. You can use column or columns - the only difference is grammar. Specify the widths of each column with the small-#, medium-#, and large-# classes.
</p>
<div class="row">
		<div class="large-12 columns" style="line-height:2rem;">
		<h3><small>Rendered</small></h3>
		  <div class="large-3 columns text-center" style="background-color:#c6c6c6">3 Columns</div>
		  <div class="large-6 columns text-center" style="background-color:#eeeeee">6 Columns</div>
		  <div class="large-3 columns text-center" style="background-color:#c6c6c6">3 Columns</div>				
		</div>
		<div class="large-12 columns">
			<h3><small>HTML</small></h3>	
			<code><textarea style="height:140px;">
<div class="row">  
  <div class="large-3 columns">3 Columns</div>
  <div class="large-6 columns">6 Columns</div>
  <div class="large-3 columns">3 Columns</div>
</div>			
			</textarea></code>
		</div>		
</div>

<h3>Targetting display sizes</h3>
<p>
	You can specify different column widths that target three different sizes: small (mobile), medium (tablet) and large (desktop). The classes can be combined such that for each display size, a different column width takes effect.   
</p>		
<div class="row">
		<div class="large-12 columns" style="line-height:2rem;">
		<h3><small>Rendered</small></h3>
			<div class="small-2 large-4 columns text-center" style="background-color:#c6c6c6">Large 4, Small 2</div>
			<div class="small-4 large-4 columns text-center" style="background-color:#eeeeee">Large 4, Small 4</div>
			<div class="small-6 large-4 columns text-center" style="background-color:#c6c6c6">Large 4, Small 6</div>
		</div>
		<div class="large-12 columns">
			<h3><small>HTML</small></h3>	
			<code><textarea style="height:140px;">
<div class="row">
  <div class="small-2 large-4 columns">Large 4, Small 2</div>
  <div class="small-4 large-4 columns">Large 4, Small 4</div>
  <div class="small-6 large-4 columns">Large 4, Small 6</div>
</div>		
			</textarea></code>
		</div>		
</div>		

<h3>Centered Columns</h3>
<p>
	Center your columns by adding a class of small-centered to your column. Large will inherit small centering by default, but you can also center solely on large by applying a large-centered class. 
</p>		
<div class="row">
		<div class="large-12 columns" style="line-height:2rem;">
		<h3><small>Rendered</small></h3>
  			<div class="small-3 small-centered columns text-center" style="background-color:#c6c6c6; float: none;">3 centered</div>
		</div>
		<div class="large-12 columns">
			<h3><small>HTML</small></h3>	
			<code><textarea style="height:140px;">
<div class="row">
  <div class="small-3 small-centered columns">3 centered</div>
</div>
			</textarea></code>
		</div>		
</div>	

<h3>Offsets</h3>
<p>
	Move blocks up to 11 columns to the right by using classes like .large-offset-1 and .small-offset-3.
</p>		
<div class="row">
		<div class="large-12 columns" style="line-height:2rem;">
		<h3><small>Rendered</small></h3>
  			<div class="large-1 columns text-center" style="background-color:#c6c6c6">1</div>
  			<div class="large-9 large-offset-2 columns text-center" style="background-color:#eeeeee">9, offset 2</div>
		</div>
		<div class="large-12 columns">
			<h3><small>HTML</small></h3>	
			<code><textarea style="height:140px;">
<div class="row">
  <div class="large-1 columns">1</div>
  <div class="large-9 large-offset-2 columns">9, offset 2</div>
</div>
			</textarea></code>
		</div>		
</div>	

<hr>	

<h2>Flex Video</h2>
<p>
	Flex Video lets browsers automatically scale video objects in your webpages. If you're embedding a video from YouTube, Vimeo, or another site that uses iframe, embed or object elements, you can wrap your video in div.flex-video to create an intrinsic ratio that will properly scale your video on any device.
</p>
<div class="row">
		<div class="large-6 columns" style="line-height:2rem;">
		<h3><small>Rendered</small></h3>
			<div class="flex-video">
			        <iframe width="420" height="315" src="//www.youtube.com/embed/aiBt44rrslw" frameborder="0" allowfullscreen></iframe>
			</div>		
		</div>
		<div class="large-6 columns">
			<h3><small>HTML</small></h3>	
			<code><textarea style="height:140px;">
<div class="flex-video">
	<iframe width="420" height="315" src="//www.youtube.com/embed/aiBt44rrslw" frameborder="0" allowfullscreen></iframe>
</div>		
			</textarea></code>
		</div>		
</div>

<hr>
<h2>Callouts</h2>
<div class="row">
		<div class="large-6 columns">
			<h3><small>Callout</small></h3>
			<div class="callout-block">
				<h5>This is a callout block.</h5>
				<p>It's a little ostentatious, but useful for important content.</p>
			</div>				
		</div>
		<div class="large-6 columns">
			<h3><small>HTML</small></h3>	
			<code><textarea style="height:140px;">
<div class="callout-block">
  <h5>This is a callout block.</h5>
  <p>It's a little ostentatious, but useful for important content.</p>
</div>		
			</textarea></code>
		</div>
</div>

<div class="row">
		<div class="large-6 columns">
			<h3><small>1/3 Callout</small></h3>
			<div class="callout-block third">
				<h5>This is a callout block.</h5>
				<p>It's a little ostentatious, but useful for important content.</p>
			</div>				
		</div>
		<div class="large-6 columns">
			<h3><small>HTML</small></h3>	
			<code><textarea style="height:140px;">
<div class="callout-block third">
  <h5>This is a callout block.</h5>
  <p>It's a little ostentatious, but useful for important content.</p>
</div>		
			</textarea></code>
		</div>
</div>

<div class="row">
		<div class="large-6 columns">
			<h3><small>Callout float right</small></h3>
			<div class="callout-block right">
				<h5>This is a callout block.</h5>
				<p>It's a little ostentatious, but useful for important content.</p>
			</div>				
		</div>
		<div class="large-6 columns">
			<h3><small>HTML</small></h3>	
			<code><textarea style="height:140px;">
<div class="callout-block right">
  <h5>This is a callout block.</h5>
  <p>It's a little ostentatious, but useful for important content.</p>
</div>		
			</textarea></code>
		</div>
</div>

<hr>
<h2>Tooltips</h2>
<p>
	Tooltips are a quick way to provide extended information on a term or action on a page. The tooltips can be positioned on the "tip-bottom", which is the default position, "tip-top", "tip-left", or "tip-right" of the target element by adding the appropriate class to them. You can even add your own custom class to style each tip differently. On a small device, the tooltips are full-width and bottom aligned.
</p>
<div class="row">
		<div class="large-6 columns" style="line-height:2rem;">
		<h3><small>Rendered</small></h3>
			<span data-tooltip class="has-tip" title="Tooltips are awesome, you should totally use them!">extended information</span>			 
		</div>
		<div class="large-6 columns">
			<h3><small>HTML</small></h3>	
			<code><textarea style="height:140px;">
<span data-tooltip class="has-tip" title="Tooltips are awesome, you should totally use them!">Hover on desktop or touch me on mobile!</span>					
			</textarea></code>
		</div>		
</div>				
	


</div><!-- end left-content_block -->
<?php endwhile; endif; ?>
</div><!-- end content -->
</div><!-- end wrapper div -->
</div><!-- end inside div -->
					
					
					
					
					
			
    
				<?php //get_sidebar(); // sidebar 1 ?>
    
		

<?php get_footer(); ?>