//Adds responsive embed to all iframes from youtube
$(document).ready(function() {

    var vidDefer = document.getElementsByTagName('iframe');
    // Remove empty P tags created by WP inside of Accordion and Orbit
    $('.accordion p:empty, .orbit p:empty').remove();
    
    $("p").find("iframe").unwrap();
    
    for (var i=0; i<vidDefer.length; i++) {
    if(vidDefer[i].getAttribute('data-src')) {
    vidDefer[i].setAttribute('src',vidDefer[i].getAttribute('data-src'));
    }
    }
    
    if(!$('iframe[src*="youtube.com"], iframe[src*="vimeo.com"]').parents("div").hasClass("sl_reveal")) {
    $('iframe[src*="youtube.com"], iframe[src*="vimeo.com"]').each(function() {
    if ( $(this).innerWidth() / $(this).innerHeight() > 1.5 ) {
    $(this).wrap("<div class='widescreen responsive-embed'/>");
    } else {
    $(this).wrap("<div class='responsive-embed'/>");
    }
    })
    }
    });

//Mailchimp Email Form scripts. The markup can be found in /parts/contetn-emailform.php
jQuery(document).ready(function($) {
    var expanded = false;

    //Checks if dropdown is open and displays or hides accordingly
    function showCheckboxes(emailNumber) {
      var checkboxes = document.getElementById("sl_form__multiselect__options--" + emailNumber);

      console.log("show", checkboxes);
      if (!expanded) {
        checkboxes.style.display = "block";
        expanded = true;
      } else {
        checkboxes.style.display = "none";
        expanded = false;
      }
    };

    //Just hides the dropdown if it's open
    function hideCheckboxes(emailNumber) {
      var checkboxes = document.getElementById("sl_form__multiselect__options--" + emailNumber);

        checkboxes.style.display = "none";
        expanded = false;
    };

    $(".sl_form__multiselect__field").focus(function(e) {
      var emailNumber = e.target.id.split("--").pop().toString();
      var optionDropdown = document.getElementById("sl_form__multiselect__field--" + emailNumber);
      var emailField = document.getElementById("mce-EMAIL--" + emailNumber);

      showCheckboxes(emailNumber);

      if (document.activeElement === optionDropdown){
        $(optionDropdown).blur();
      }

      $(emailField).on('focus', function() {
       hideCheckboxes(emailNumber);
      })

      var selectOptions = '#sl_form__multiselect__options--' + emailNumber + ' input:checkbox'

    $(selectOptions).on("change", function(){
      var whoOptions = [];
      $(selectOptions).each(function(){
       if($(this).is(":checked")){
         whoOptions.push(this.parentNode.textContent)
         document.getElementById("sl_who-input--" + emailNumber).innerHTML = whoOptions.concat();
         document.getElementById("sl_who-input--" + emailNumber).style.color = "#152435";
       }

       //If more than one option is selected
       if(whoOptions.length > 1){
        document.getElementById("sl_who-input--" + emailNumber).innerHTML = "Multiple Selected";
        document.getElementById("sl_who-input--" + emailNumber).style.color = "#152435";
       }

       //If less than one option is selected, reformat to initial state
       if(whoOptions.length < 1 || whoOptions == undefined){
        document.getElementById("sl_who-input--" + emailNumber).innerHTML = "I am...";
        document.getElementById("sl_who-input--" + emailNumber).style.color = "inherit";
       }
      })   
    });
    });
});
//END MAILCHIMP JS