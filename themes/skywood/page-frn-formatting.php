<?php
/*
Template Name: FRN Formatting
*/
?>

<?php get_header(); ?>
			
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			<?php 
				$post_thumbnail_id = get_post_thumbnail_id();
				$featured_src = wp_get_attachment_image_src( $post_thumbnail_id, 'full' );
				$featured_mobile_src = get_field( 'mobile_featured_image');
			?>
<?php 
// This is where all of the sample formatting stuff goes
// Please note the code in the <textarea> requires no formatting/spacing tabs.
?>

<style>code{background: none; border: none;}textarea{overflow: hidden; overflow-y: scroll;}</style>
<br>

<div id="main">

<div class="row">
	<div class="large-6 columns">
		<h1>h1. This is a very large header.</h1>
		<h2>h2. This is a large header.</h2>
		<h3>h3. This is a medium header.</h3>
		<h4>h4. This is a moderate header.</h4>
		<h5>h5. This is a small header.</h5>
		<h6>h6. This is a tiny header.</h6>
	</div><!-- /.col columns -->
	<div class="large-6 columns">
		<code class="clearfix">
			<textarea style="height: 200px;">
<h1>h1. This is a very large header.</h1>
<h2>h2. This is a large header.</h2>
<h3>h3. This is a medium header.</h3>
<h4>h4. This is a moderate header.</h4>
<h5>h5. This is a small header.</h5>
<h6>h6. This is a tiny header.</h6>
			</textarea>
		</code>
	</div><!-- /.col columns -->
</div><!-- /.row -->

<div class="sl_divider divider"></div>

<div class="row">
	<div class="large-6 columns">

<div class="sl_divider divider"></div>

	</div><!-- /.col columns -->
	<div class="large-6 columns">
		<code class="clearfix">
			<textarea style="height: 30px;">
[divider]
			</textarea>
		</code>
	</div><!-- /.col columns -->
</div><!-- /.row -->

<div class="sl_divider divider"></div>

<div class="row">
	<div class="medium-4 columns">
	<h2>Column Heading</h2>
	Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.
	</div>	<!-- /.col columns -->

	<div class="medium-4 columns">
	<h2>Column Heading</h2>
	Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.
	</div>	<!-- /.col columns -->

	<div class="medium-4 columns">
	<h2>Column Heading</h2>
	Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.
	</div>	<!-- /.col columns -->
</div><!-- /.row -->


<div class="row">
	<div class="small-12 columns">
			<code class="clearfix">
			<textarea style="height: 500px;">
[row]
[col large="4"]
<h2>Column Heading</h2>
Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.
[/col]

[col large="4"]
<h2>Column Heading</h2>
Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.
[/col]

[col large="4"]
<h2>Column Heading</h2>
Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.
[/col]
[/row]
		</textarea>
		</code>
	</div><!-- /.col columns -->
</div><!-- /.row -->

<div class="sl_divider divider"></div>

<div class="row">
	<div class="large-6 columns">
		<br>
		<blockquote>“Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam eleifend turpis dui. Fusce id interdum dui, ac semper risus. Curabitur sed fermentum lorem. Vestibulum vitae efficitur augue, nectw.” <cite>Noah benShea</cite></blockquote>
		<blockquote class="secondary">“Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam eleifend turpis dui. Fusce id interdum dui, ac semper risus. Curabitur sed fermentum lorem. Vestibulum vitae efficitur augue, nectw.” <cite>Noah benShea</cite></blockquote>
	</div><!-- /.col columns -->
	<div class="large-6 columns">
		<code class="clearfix">
			<textarea style="height: 125px;">
<blockquote>“Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam eleifend turpis dui. Fusce id interdum dui, ac semper risus. Curabitur sed fermentum lorem. Vestibulum vitae efficitur augue, nectw.”
<cite>Noah benShea</cite></blockquote>
			</textarea>
		</code>
	</div><!-- /.col columns -->
</div><!-- /.row -->

<div class="sl_divider divider"></div>

<div class="row">
	<div class="large-6 columns">
		<br>
		<h4>Un-ordered Lists</h4>
		<ul>
		 	<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam beatae consequatur quam.</li>
		 	<li>Deserunt, recusandae veritatis illum perspiciatis illo quae odio laboriosam laudantium consequuntur eveniet? Magni, tempore, animi.</li>
		 	<li>Tempora, laboriosam, aspernatur, quisquam, recusandae pariatur hic dignissimos a inventore magni cupiditate quis eius. Nobis.</li>
		</ul>
		<ul class="secondary">
		 	<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam beatae consequatur quam.</li>
		 	<li>Deserunt, recusandae veritatis illum perspiciatis illo quae odio laboriosam laudantium consequuntur eveniet? Magni, tempore, animi.</li>
		 	<li>Tempora, laboriosam, aspernatur, quisquam, recusandae pariatur hic dignissimos a inventore magni cupiditate quis eius. Nobis.</li>
		</ul>
		<ul class="disc">
		 	<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam beatae consequatur quam.</li>
		 	<li>Deserunt, recusandae veritatis illum perspiciatis illo quae odio laboriosam laudantium consequuntur eveniet? Magni, tempore, animi.</li>
		 	<li>Tempora, laboriosam, aspernatur, quisquam, recusandae pariatur hic dignissimos a inventore magni cupiditate quis eius. Nobis.</li>
		</ul>
	</div><!-- /.col columns -->
	<div class="large-6 columns">
		<code class="clearfix">
			<textarea style="height: 600px;">
<h4>Un-ordered Lists</h4>
<ul>
<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam beatae consequatur quam.</li>
<li>Deserunt, recusandae veritatis illum perspiciatis illo quae odio laboriosam laudantium consequuntur eveniet? Magni, tempore, animi.</li>
<li>Tempora, laboriosam, aspernatur, quisquam, recusandae pariatur hic dignissimos a inventore magni cupiditate quis eius. Nobis.</li>
</ul>
<ul class="secondary">
<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam beatae consequatur quam.</li>
<li>Deserunt, recusandae veritatis illum perspiciatis illo quae odio laboriosam laudantium consequuntur eveniet? Magni, tempore, animi.</li>
<li>Tempora, laboriosam, aspernatur, quisquam, recusandae pariatur hic dignissimos a inventore magni cupiditate quis eius. Nobis.</li>
</ul>
<ul class="disc">
<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam beatae consequatur quam.</li>
<li>Deserunt, recusandae veritatis illum perspiciatis illo quae odio laboriosam laudantium consequuntur eveniet? Magni, tempore, animi.</li>
<li>Tempora, laboriosam, aspernatur, quisquam, recusandae pariatur hic dignissimos a inventore magni cupiditate quis eius. Nobis.</li>
</ul>
			</textarea>
		</code>
	</div><!-- /.col columns -->
</div><!-- /.row -->

<div class="sl_divider divider"></div>

<div class="row">
	<div class="large-6 columns">
		<br>
		<h4>Ordered Lists</h4>
		<ol>
		 	<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam beatae consequatur quam.</li>
		 	<li>Deserunt, recusandae veritatis illum perspiciatis illo quae odio laboriosam laudantium consequuntur eveniet? Magni, tempore, animi.</li>
		 	<li>Tempora, laboriosam, aspernatur, quisquam, recusandae pariatur hic dignissimos a inventore magni cupiditate quis eius. Nobis.</li>
		</ol>
	</div><!-- /.col columns -->
	<div class="large-6 columns">
		<code class="clearfix">
			<textarea style="height: 275px;">
<h4>Ordered Lists</h4>
<ol>
<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam beatae consequatur quam.</li>
<li>Deserunt, recusandae veritatis illum perspiciatis illo quae odio laboriosam laudantium consequuntur eveniet? Magni, tempore, animi.</li>
<li>Tempora, laboriosam, aspernatur, quisquam, recusandae pariatur hic dignissimos a inventore magni cupiditate quis eius. Nobis.</li>
</ol>
			</textarea>
		</code>
	</div><!-- /.col columns -->
</div><!-- /.row -->

<div class="sl_divider divider"></div>

<div class="row">
	<div class="large-6 columns">
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam. Consectetur adipisicing elit. Omnis, optio, voluptatum veniam.</p>
		<div class="sl_callout callout no-box">
			Omnis, optio, voluptatum veniam. Consectetur adipisicing elit. Omnis, optio, voluptatum veniam.
		</div>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam. Consectetur adipisicing elit. Omnis, optio, voluptatum veniam.</p>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam. Consectetur adipisicing elit. Omnis, optio, voluptatum veniam.</p>
		<div class="sl_callout callout no-box-large">
			Omnis, optio, voluptatum veniam. Consectetur adipisicing elit. Omnis, optio, voluptatum veniam.
		</div>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam. Consectetur adipisicing elit. Omnis, optio, voluptatum veniam.</p>
	</div><!-- /.col columns -->
	<div class="large-6 columns">
		<code class="clearfix">
			<textarea style="height: 700px;">
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam. Consectetur adipisicing elit. Omnis, optio, voluptatum veniam.</p>
[callout style="no-box"]
Omnis, optio, voluptatum veniam. Consectetur adipisicing elit. Omnis, optio, voluptatum veniam.
[/callout]
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam. Consectetur adipisicing elit. Omnis, optio, voluptatum veniam.</p>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam. Consectetur adipisicing elit. Omnis, optio, voluptatum veniam.</p>
[callout style="no-box-large"]
Omnis, optio, voluptatum veniam. Consectetur adipisicing elit. Omnis, optio, voluptatum veniam.
[/callout]
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam. Consectetur adipisicing elit. Omnis, optio, voluptatum veniam.</p>
			</textarea>
		</code>
	</div><!-- /.col columns -->
</div><!-- /.row -->

<div class="sl_divider divider"></div>

<div class="row">
	<div class="large-6 columns">
		<div class="sl_callout callout">
			<h4>H4. This is the Default Callout</h4>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam. Consectetur adipisicing elit. Omnis, optio, voluptatum veniam.</p>
		</div>
		<div class="sl_callout callout secondary">
			<h4>H4. This is the Secondary Callout</h4>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam. Consectetur adipisicing elit. Omnis, optio, voluptatum veniam.</p>
		</div>
		<div class="sl_callout callout inverse">
			<h4>H4. This is an Inverse Callout</h4>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam. Consectetur adipisicing elit. Omnis, optio, voluptatum veniam.</p>
		</div>
		<div class="sl_callout-card callout-card">
			<div class="card-media" style="background-image: url(/wp-content/uploads/skywood-facility.jpg)"></div>
			<div class="card-content">
				<h4>This is a Callout Card</h4>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam. Consectetur adipisicing elit. Omnis, optio, voluptatum veniam.</p>
			</div>
		</div>
		<div class="sl_callout-card callout-card secondary">
			<div class="card-media" style="background-image: url(/wp-content/uploads/skywood-facility.jpg)"></div>
			<div class="card-content">
				<h4>This is a Callout Card</h4>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam. Consectetur adipisicing elit. Omnis, optio, voluptatum veniam.</p>
			</div>
		</div>
		<div class="sl_callout-card callout-card inverse">
			<div class="card-media" style="background-image: url(/wp-content/uploads/skywood-facility.jpg)"></div>
			<div class="card-content">
				<h4>This is a Callout Card</h4>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam. Consectetur adipisicing elit. Omnis, optio, voluptatum veniam.</p>
			</div>
		</div>
	</div><!-- /.col columns -->
	<div class="large-6 columns">
		<code class="clearfix">
			<textarea style="height: 1000px;">
[callout]
<h4>H4. This is the Default Callout</h4>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam. Consectetur adipisicing elit. Omnis, optio, voluptatum veniam.</p>
[/callout]
[callout style="secondary"]
<h4>H4. This is the Secondary Callout</h4>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam. Consectetur adipisicing elit. Omnis, optio, voluptatum veniam.</p>
[/callout]
[callout style="inverse"]
<h4>H4. This is an Inverse Callout</h4>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam. Consectetur adipisicing elit. Omnis, optio, voluptatum veniam.</p>
[/callout]
[callout-card img="/wp-content/uploads/skywood-facility.jpg"]
<h4>This is a Callout Card</h4>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam. Consectetur adipisicing elit. Omnis, optio, voluptatum veniam.</p>
[/callout-card]
[callout-card img="/wp-content/uploads/skywood-facility.jpg" style="secondary"]
<h4>This is a Callout Card</h4>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam. Consectetur adipisicing elit. Omnis, optio, voluptatum veniam.</p>
[/callout-card]
[callout-card img="/wp-content/uploads/skywood-facility.jpg" style="inverse"]
<h4>This is a Callout Card</h4>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam. Consectetur adipisicing elit. Omnis, optio, voluptatum veniam.</p>
[/callout-card]
			</textarea>
		</code>
	</div><!-- /.col columns -->
</div><!-- /.row -->

<div class="sl_divider divider"></div>

<div class="row" data-equalizer data-equalize-on="medium">
	<div class="large-6 columns">
		<div class="sl_callout callout equalizer" data-equalizer-watch>
		<h4>This is a callout equalizer box</h4>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam beatae consequatur quam. Deserunt, recusandae veritatis illum perspiciatis illo quae odio laboriosam laudantium consequuntur eveniet? Magni, tempore, animi.</p>
		<p><a href="#" class="sl_button button " target="_self">Find Out More</a></p>
		</div>
	</div><!-- /.col columns -->
		<div class="large-6 columns">
		<div class="sl_callout callout equalizer" data-equalizer-watch>
		<h4>This is a callout equalizer box</h4>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam beatae consequatur quam.</p>
		<p><a href="#" class="sl_button button " target="_self">Find Out More</a></p>
		</div>
	</div><!-- /.col columns -->
	<div class="large-12 columns">
		<code class="clearfix">
			<textarea style="height: 500px;">
[callout_equalizer]
<h4>This is a callout equalizer box</h4>
<ul>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam beatae consequatur quam. Deserunt, recusandae veritatis illum perspiciatis illo quae odio laboriosam laudantium consequuntur eveniet? Magni, tempore, animi.</p>
[button url="#"]Find Out More[/button]
[/callout_equalizer]

[callout_equalizer]
<h4>This is a callout equalizer box</h4>
<ul>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam beatae consequatur quam.</p>
[button url="#"]Find Out More[/button]
[/callout_equalizer]
			</textarea>
		</code>
	</div><!-- /.col columns -->
</div><!-- /.row -->

<div class="sl_divider divider"></div>

<div class="row">
	<div class="large-5 columns">
		<p><a href="#" class="sl_button button " target="_self">Button</a> <a href="#" class="sl_button button secondary" target="_self">Button</a> <a href="#" class="sl_button button inverse" target="_self">Button</a></p>
		<div class="sl_read-next--button">
			<h4>Read This Next:</h4>
			<p><a href="#" class="sl_button button secondary read-next" target="_self">Long Article Title</a></p>
		</div>
	</div><!-- /.col columns -->
	<div class="large-7 columns">
			<textarea style="height: 200px;">
[button url="#"]Button[/button]
[button url="#" style="secondary"]Button[/button]
[button url="#" style="inverse"]Button[/button]
[button url="#" style="read-next"]Long Article Title[/button]
			</textarea>
	</div><!-- /.col columns -->
</div><!-- /.row -->

<div class="sl_divider divider"></div>

<div class="sl_row row">
	<div class="large-6 sl_columns columns">
		<?php echo do_shortcode('[read-next url="#" title="Link title goes here"]This is the preview text...[/read-next]')?>
		<?php echo do_shortcode('[read-next style="large" url="#" title="Link title goes here"]This is the preview text...[/read-next]')?>
	</div><!-- /.col columns -->

	<div class="large-6 sl_columns columns">
			<textarea style="height: 100px;">
[read-next url="#" title="Link title goes here"]This is the preview text...[/read-next]
[read-next style="large" url="#" title="Link title goes here"]This is the preview text...[/read-next]
			</textarea>
	</div><!-- /.col columns -->

</div><!-- /.row -->

<div class="sl_divider divider"></div>

<div class="row">
	<div class="large-6 columns">
<ul class="sl_tabs tabs " data-tabs="5yiap6-tabs" id="example-tabs1">
<li class="sl_tabs-title tabs-title is-active" role="presentation"><a href="#tabpanel1" role="tab" aria-controls="tabpanel1" aria-selected="true" id="tabpanel1-label">Tab One Title</a></li>
<li class="sl_tabs-title tabs-title " role="presentation"><a href="#tabpanel2" role="tab" aria-controls="tabpanel2" aria-selected="false" id="tabpanel2-label">Tab Two Title </a></li>
<li class="sl_tabs-title tabs-title " role="presentation"><a href="#tabpanel3" role="tab" aria-controls="tabpanel3" aria-selected="false" id="tabpanel3-label">Tab Three Title </a></li>
</ul>
<div class="sl_tabs-content tabs-content" data-tabs-content="example-tabs1">
<div class="sl_tabs-panel tabs-panel is-active" id="tabpanel1" role="tabpanel" aria-hidden="false" aria-labelledby="tabpanel1-label">
<h4>Tab 1 Content</h4>
<p>This content is exclusive to tab one. Place your tab one content here.</p>
</div>
<div class="sl_tabs-panel tabs-panel " id="tabpanel2" role="tabpanel" aria-hidden="true" aria-labelledby="tabpanel2-label">
<h4>Tab 2 Content</h4>
<p>This content is exclusive to tab two. Place your tab two content here.</p>
</div>
<div class="sl_tabs-panel tabs-panel " id="tabpanel3" role="tabpanel" aria-hidden="true" aria-labelledby="tabpanel3-label">
<h4>Tab 3 Content</h4>
<p>This content is exclusive to tab three. Place your tab three content here.</p>
</div>
</div>
<ul class="sl_tabs tabs secondary" data-tabs="5yiap6-tabs" id="example-tabs2">
<li class="sl_tabs-title tabs-title is-active" role="presentation"><a href="#tabpanel4" role="tab" aria-controls="tabpanel4" aria-selected="true" id="tabpanel1-label">Tab One Title</a></li>
<li class="sl_tabs-title tabs-title " role="presentation"><a href="#tabpanel5" role="tab" aria-controls="tabpanel5" aria-selected="false" id="tabpanel2-label">Tab Two Title </a></li>
<li class="sl_tabs-title tabs-title " role="presentation"><a href="#tabpanel6" role="tab" aria-controls="tabpanel6" aria-selected="false" id="tabpanel3-label">Tab Three Title </a></li>
</ul>
<div class="sl_tabs-content tabs-content secondary" data-tabs-content="example-tabs1">
<div class="sl_tabs-panel tabs-panel is-active" id="tabpanel4" role="tabpanel" aria-hidden="false" aria-labelledby="tabpanel4-label">
<h4>Tab 1 Content</h4>
<p>This content is exclusive to tab one. Place your tab one content here.</p>
</div>
<div class="sl_tabs-panel tabs-panel " id="tabpanel5" role="tabpanel" aria-hidden="true" aria-labelledby="tabpanel5-label">
<h4>Tab 2 Content</h4>
<p>This content is exclusive to tab two. Place your tab two content here.</p>
</div>
<div class="sl_tabs-panel tabs-panel " id="tabpanel6" role="tabpanel" aria-hidden="true" aria-labelledby="tabpanel6-label">
<h4>Tab 3 Content</h4>
<p>This content is exclusive to tab three. Place your tab three content here.</p>
</div>
</div>
	</div><!-- /.col columns -->
	<div class="large-6 columns">
			<textarea style="height: 570px;">
[tabs]
[tab-title]Tab One Title[/tab-title]
[tab-title]Tab Two Title [/tab-title]
[tab-title]Tab Three Title [/tab-title]
[/tabs]

[tab-content]
[tab-panel]
<h2>Tab 1 Content</h2>
This content is exclusive to tab one. Place your tab one content here.

[/tab-panel]

[tab-panel]
<h2>Tab 2 Content</h2>
This content is exclusive to tab two. Place your tab two content here.

[/tab-panel]

[tab-panel]
<h2>Tab 3 Content</h2>
This content is exclusive to tab three. Place your tab three content here.

[/tab-panel]
[/tab-content]
			</textarea>
	</div><!-- /.col columns -->
</div><!-- /.row -->

<div class="sl_divider divider"></div>

<div class="row">
	<div class="large-6 columns">
		<ul class="sl_accordion accordion" data-accordion="l4eitt-accordion" data-multi-expand="true" data-allow-all-closed="true" role="tablist">
			<li class="sl_accordion-item accordion-item" data-accordion-item=""><a href="#" class="sl_accordion-title accordion-title" aria-controls="panel1" role="tab" id="panel1-label" aria-expanded="false" aria-selected="false">Accordian Title 1</a>
				<div id="panel1" class="sl_accordion-content accordion-content" data-tab-content="" role="tabpanel" aria-labelledby="panel1-label" aria-hidden="true"><!-- start accordion content --->
					<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper.</p>
				</div>
			</li>
			<li class="sl_accordion-item accordion-item" data-accordion-item=""><a href="#" class="sl_accordion-title accordion-title" aria-controls="panel2" role="tab" id="panel2-label" aria-expanded="false" aria-selected="false">Accordian Title 2</a>
				<div id="panel2" class="sl_accordion-content accordion-content" data-tab-content="" role="tabpanel" aria-labelledby="panel2-label" aria-hidden="true">
					<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper.</p>
				</div>
			</li>
			<li class="sl_accordion-item accordion-item" data-accordion-item=""><a href="#" class="sl_accordion-title accordion-title" aria-controls="panel3" role="tab" id="panel3-label" aria-expanded="false" aria-selected="false">Accordian Title 3</a>
				<div id="panel3" class="sl_accordion-content accordion-content" data-tab-content="" role="tabpanel" aria-labelledby="panel3-label" aria-hidden="true">
					<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper.</p>
				</div>
			</li>
		</ul>

		<ul class="sl_accordion accordion secondary" data-accordion="l4eitt-accordion" data-multi-expand="true" data-allow-all-closed="true" role="tablist">
			<li class="sl_accordion-item accordion-item" data-accordion-item=""><a href="#" class="sl_accordion-title accordion-title" aria-controls="panel1" role="tab" id="panel1-label" aria-expanded="false" aria-selected="false">Accordian Title 1</a>
				<div id="panel1" class="sl_accordion-content accordion-content" data-tab-content="" role="tabpanel" aria-labelledby="panel1-label" aria-hidden="true"><!-- start accordion content --->
					<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper.</p>
				</div>
			</li>
			<li class="sl_accordion-item accordion-item" data-accordion-item=""><a href="#" class="sl_accordion-title accordion-title" aria-controls="panel2" role="tab" id="panel2-label" aria-expanded="false" aria-selected="false">Accordian Title 2</a>
				<div id="panel2" class="sl_accordion-content accordion-content" data-tab-content="" role="tabpanel" aria-labelledby="panel2-label" aria-hidden="true">
					<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper.</p>
				</div>
			</li>
			<li class="sl_accordion-item accordion-item" data-accordion-item=""><a href="#" class="sl_accordion-title accordion-title" aria-controls="panel3" role="tab" id="panel3-label" aria-expanded="false" aria-selected="false">Accordian Title 3</a>
				<div id="panel3" class="sl_accordion-content accordion-content" data-tab-content="" role="tabpanel" aria-labelledby="panel3-label" aria-hidden="true">
					<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper.</p>
				</div>
			</li>
		</ul>
	</div><!-- /.col columns -->

	<div class="large-6 columns">
			<textarea style="height: 500px;">
[accordion]
[accordion-item title="Accordian Title 1"]
<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper.</p>
[/accordion-item]
[accordion-item title="Accordian Title 2"]
<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper.</p>[/accordion-item]
[accordion-item title="Accordian Title 3"]
<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper.</p>
[/accordion-item]
[/accordion]

[accordion style="secondary"]
[accordion-item title="Accordian Title 1"]
<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper.</p>
[/accordion-item]
[accordion-item title="Accordian Title 2"]
<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper.</p>[/accordion-item]
[accordion-item title="Accordian Title 3"]
<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper.</p>
[/accordion-item]
[/accordion]
			</textarea>
	</div><!-- /.col columns -->

</div><!-- /.row -->

<div class="sl_divider divider"></div>

<div class="sl_row row">
	<div class="large-8 sl_columns columns">
		<?php echo do_shortcode('[email not_content="true"]'); ?>
		<?php echo do_shortcode('[email heading="You can customize this" campaign="specific_campaign" not_content="true"]'); ?>
	</div><!-- /.col columns -->

	<div class="large-4 sl_columns columns">
			<textarea style="height: 300px;">
[email]
[email heading="You can customize this" campaign="specific_campaign"]
			</textarea>
	</div><!-- /.col columns -->

</div><!-- /.row -->
</div>

<?php endwhile; endif; ?>
</div><!-- end content -->
</div><!-- end wrapper div -->
</div><!-- end inside div -->
					
					
					
					
					
			
    
				<?php //get_sidebar(); // sidebar 1 ?>
    
		

<?php get_footer(); ?>