<?php
/*
Template Name: Homepage-Button
*/
?>

<?php get_header(); ?>
			
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			<?php 
				$post_thumbnail_id = get_post_thumbnail_id();
				$featured_src = wp_get_attachment_image_src( $post_thumbnail_id, 'full' );
				$featured_mobile_src = get_field( 'mobile_featured_image');
			?>
			<div id="home-banner-outer">
				<div id="home-banner" class="stretchy-wrapper home-parallax-bg">
					<div id="home-words" class="stretchy-filler"></div>
					<a href="/contact/" id="home-words" class="button secondary large home">Contact Us</a>
					<div id="home-trees" class="stretchy-filler"></div>
				</div>
			</div>
			<div id="home-banner-mobile" class="stretchy-wrapper test">
				<div></div>
			</div>
			

					
					<div id="home-first-section">
						<div id="swirly"></div>
						<div id="gray-content">
							<div class="row">
								<div class="large-10 large-centered columns text-center">
									<h2 class="section-title"><?php echo get_field('gray_title'); ?></h2>
									<h3><?php echo get_field('gray_quote'); ?></h3>
									<p><?php echo get_field('gray_speaker'); ?></p>
								</div>
							</div>
							<div class="row">
								<div class="large-6 large-centered columns text-center">
									<?php echo get_field('gray_content'); ?>
									<a href="<?php echo get_field('gray_button_url'); ?>" class="button secondary large"><?php echo get_field('gray_button_text'); ?></a>
								</div>
							</div>
						</div>
					</div>
					<div id="assess-contact" class="tb-pad-90">
						<div class="row">
							<div class="large-12 columns">
								<h2 class="section-title">Take an assessment</h2>
								<p id="assess-subtitle" class="text-center primary-color">AND SEND RESULTS TO AN ADMISSIONS COORDINATOR</p>
							</div>
						</div>
						<div class="row ">
							<div class="medium-6 columns">
								<div id="assessment-shell">

								</div>
							</div>
							<div class="medium-6 columns" id="home-contact-form">
								<?php 
									$contact_form = get_field('contact_form_7_shortcode');
									echo do_shortcode( $contact_form );
								?>
							</div>
						</div>
					</div>
					
					<?php 
					
					//video testimonialtype
					$args = array(
						'post_type'=>'testimonial',
						'orderby'=>'menu_order',
						'order'=>'asc'
					);
					$testimonials = new WP_Query( $args );
					if($testimonials->have_posts()) : 
						?>
						
						<div id="video-testimonials">
							<div class="row tb-pad-60">
								<div class="small-12 columns">
									<h2 class="section-title">
										Watch Video
									</h2>
									<div class="row text-center">
										<div class="large-8 medium-10 medium-centered columns">
											<h3><?php echo get_field('video_quote'); ?></h3>
											<p><?php echo get_field('video_quote_speaker'); ?></p>
										</div>
									</div>
									<div class="row">
										<div class="large-6 medium-8 medium-centered columns">
											<div class="row tb-pad-20 small-up-1">
												<?php 
												while($testimonials->have_posts()) : 
													$testimonials->the_post();	
													$title = get_the_title();
													$feat_image = wp_get_attachment_url( get_post_thumbnail_id() );
													$video = get_field('video_url');
													?>
													<div class="column">
														<a class="fancybox-media" href="<?php echo $video ?>">
															<span class="vid-test-thumb-box" style="background: url('<?php echo $feat_image ?>') no-repeat center center / cover">
																<span class="vid-play-icon"></span>
															</span>
														</a>
													</div>
													<?php
													
													
												endwhile;
												?>
											</div><!-- end row -->
										</div>
									</div>
								</div>
							</div>
						</div>
						
						<?php 
					endif;
					
					?>
					<?php 
					//insurance content type
					$args = array(
						'post_type'=>'hp_treatment',
						'orderby'=>'menu_order',
						'order'=>'asc'
					);
					$treatments = new WP_Query( $args );
					if($treatments->have_posts()) : 
						?>
						<div id="treatment-section">
							<div class=" tb-pad-60" style="padding-bottom: 0;">
								<div class="col-sm-12">
									<div id="treatment-section-leadin">
										<h2 class="section-title">
											OUR APPROACH TO RECOVERY
										</h2>
										<?php 
											if(get_field('treatment_subheading')){
												the_field('treatment_subheading');
											}
										?>
									</div>
									<?php 
									$treatment_count = 0;
									while($treatments->have_posts()) : 
										$treatment_count++;
										$treatments->the_post();	
										$title = get_the_title();
										$feat_image = wp_get_attachment_url( get_post_thumbnail_id() );
										?>
										<div class="row expanded collapse" data-equalizer data-equalize-on="medium">
											<div class="medium-6 columns <?php echo ($treatment_count %2 == 0) ? 'float-right' : '' ; ?>" >
												<div class="treatment-feature-image-box" data-equalizer-watch>
													<div class="treatment-feature-image" style="background: url('<?php echo $feat_image ?>') no-repeat center center / cover; transition: all 2.5s ease; -moz-transition: all 2.5s ease; -ms-transition: all 2.5s ease; -webkit-transition: all 2.5s ease; -o-transition: all 2.5s ease;" data-equalizer-watch>
												</div>
												</div>
											</div>
											<div class="medium-6 columns <?php echo ($treatment_count %2 == 0) ? 'float-left text-right' : '' ; ?>" data-equalizer-watch>
												<div class="treatment-pad">
													<div class="large-8 columns <?php echo ($treatment_count %2 == 0) ? 'float-right' : '' ; ?>">
														<div class="title">
															<h3><?php echo $title; ?></h3>
														</div>
														<div class="content">
															<?php the_content() ?>
														</div>
														<div class="button-shell">
															<?php 
																$page_url = get_field('page_url');
															//var_dump( $obj );
																
																?>
							
															<a  class="button" href="<?php echo $page_url ?>">
																Learn More
															</a>
																<?php 
																
															?>
														</div>
													</div>
													<div class="clearfix"></div>
												</div>
											</div>
										<?php
										?>
										</div><!-- end row -->
									<?php
									endwhile;
									?>
								</div>
							</div>
							
						</div>
						<?php 
					endif;

					
					//insurance content type
					$args = array(
						'post_type'=>'insurance',
						'orderby'=>'menu_order',
						'order'=>'asc'
					);
					$insurances = new WP_Query( $args );
					if($insurances->have_posts()) : 
						?>
						<div id="insurance-section" class="tb-pad-90">
							<div class="row">
								<div class="small-12 columns">
									<h2 class="primary-color text-center">
										WE ACCEPT THESE INSURANCES AND MORE
									</h2>
								</div>
							</div>
							<div class="tb-pad-20">
								<div class="row small-up-2 medium-up-2 large-up-4">
									<?php 
									while($insurances->have_posts()) : 
										$insurances->the_post();	
										$title = get_the_title();
										$feat_image = wp_get_attachment_url( get_post_thumbnail_id() );
										?>
										<div class="column" style="padding-bottom: 20px;">
											<img class="insurance-logo" alt="<?php echo $title; ?>" src="<?php echo $feat_image ?>"/>
										</div>
										<?php
										
										
									endwhile;
									?>
								</div><!-- end row -->
							</div>
							<div class="row">
								<div class="large-12 columns text-center"><a href="<?php echo site_url(); ?>/admissions/insurance-coverage/" class="button">Learn More</a></div>
							</div>
						</div>
						<?php 
					endif;
					
	
					?>
					
					
					
					
					
			
    
				<?php //get_sidebar(); // sidebar 1 ?>
    
			
			<?php endwhile; ?>	
					
					
			<?php endif; ?>

<?php get_footer(); ?>