jQuery(document).ready(function( $ ){
    $('.sl_gallery__slider').slick({
        // normal options...
        arrows: false,
        dots:true,
        swipeToSlide: true,
        infinite: true,
        centerMode: true,
        slidesToShow: 1,
        autoplay: true,
        autoplaySpeed: 6000,
        // the magic
        responsive: [{

            breakpoint: 1024,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
            }

        }, {

            breakpoint: 767,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                swipeToSlide: true,
                arrows: false,
            }

        }]
    });

    $('.sl_carousel__slider').slick({
        // normal options...
        prevArrow: `<button class="sl_button--carousel sl_prev"></button>`,
        nextArrow: `<button class="sl_button--carousel sl_next"></button>`,
        dots:true,
        swipeToSlide: true,
        infinite: true,
        // the magic here
        responsive: [{

            breakpoint: 1024,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
            }

        }, {

            breakpoint: 767,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                swipeToSlide: true,
                arrows: true,
            }

        }]
    });
});