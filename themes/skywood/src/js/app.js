import $ from 'jquery'
import 'slick-carousel/slick/slick'

import Foundation from 'foundation-sites'

$(document).foundation();

import slickCarousel from './lib/slick-carousel'