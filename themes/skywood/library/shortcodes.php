<?php

// shortcodes


// Buttons
function buttons( $atts, $content = null ) {
	extract( shortcode_atts( array(
	'type' => 'default', /* primary, default, info, success, danger, warning, inverse */
	'size' => 'default', /* mini, small, default, large */
	'url'  => '',
	'text' => '', 
	), $atts ) );
	
	if($type == "default"){
		$type = "";
	}
	else{ 
		$type = "btn-" . $type;
	}
	
	if($size == "default"){
		$size = "";
	}
	else{
		$size = "btn-" . $size;
	}
	
	$output = '<a href="' . $url . '" class="btn '. $type . ' ' . $size . '">';
	$output .= $text;
	$output .= '</a>';
	
	return $output;
}

add_shortcode('button', 'buttons'); 

// Alerts
function alerts( $atts, $content = null ) {
	extract( shortcode_atts( array(
	'type' => 'alert-info', /* alert-info, alert-success, alert-error */
	'close' => 'false', /* display close link */
	'text' => '', 
	), $atts ) );
	
	$output = '<div class="fade in alert alert-'. $type . '">';
	if($close == 'true') {
		$output .= '<a class="close" data-dismiss="alert">×</a>';
	}
	$output .= $text . '</div>';
	
	return $output;
}

add_shortcode('alert', 'alerts');

// Block Messages
function block_messages( $atts, $content = null ) {
	extract( shortcode_atts( array(
	'type' => 'alert-info', /* alert-info, alert-success, alert-error */
	'close' => 'false', /* display close link */
	'text' => '', 
	), $atts ) );
	
	$output = '<div class="fade in alert alert-block alert-'. $type . '">';
	if($close == 'true') {
		$output .= '<a class="close" data-dismiss="alert">×</a>';
	}
	$output .= '<p>' . $text . '</p></div>';
	
	return $output;
}

add_shortcode('block-message', 'block_messages'); 

// Block Messages
function blockquotes( $atts, $content = null ) {
	extract( shortcode_atts( array(
	'float' => '', /* left, right */
	'cite' => '', /* text for cite */
	), $atts ) );
	
	$output = '<blockquote';
	if($float == 'left') {
		$output .= ' class="pull-left"';
	}
	elseif($float == 'right'){
		$output .= ' class="pull-right"';
	}
	$output .= '><p>' . $content . '</p>';
	
	if($cite){
		$output .= '<small>' . $cite . '</small>';
	}
	
	$output .= '</blockquote>';
	
	return $output;
}

add_shortcode('blockquote', 'blockquotes'); 
 

// Shortcodes based on Foundations 6.0

	// ROW
		function frn_foundation_row ( $atts, $content = null ) {
			$specs = shortcode_atts( array(
				'style'		=> 'small-collapse large-uncollapse medium-uncollapse'
				), $atts );
			return '<div class="sl_row row expanded content-shortcode-row ' . esc_attr($specs['style'] ) . '" data-equalizer data-equalize-on="medium">' . do_shortcode ( $content ) . '</div>';
		}
		add_shortcode ('row', 'frn_foundation_row' );
	///ROW

	// COLUMN
		function frn_foundation_col ( $atts, $content = null ) {
			$specs = shortcode_atts( array(
				'small'		=> '12',
				'medium'	=> '12',
				'large'		=> '12',
				), $atts );
			return '<div class="small-' . esc_attr($specs['small']) . ' medium-' . esc_attr($specs['medium']) . ' large-' . esc_attr($specs['large']) . ' columns sl_columns">' . do_shortcode ( $content ) . '</div>';
		}
		add_shortcode ('col', 'frn_foundation_col' );
	///COLUMN

	// BLOCKGRID
		function frn_foundation_blockgridul ( $atts, $content = null ) {
			$specs = shortcode_atts( array(
				'screen'		=> 'medium',
				'columns'		=> '4',
				'altscreen'		=> '',
				'altcolumns'	=> '',
				), $atts );
			return '<div class="sl_row row ' . esc_attr($specs['screen'] ) . '-up-' . esc_attr($specs['columns']) . ' ' . esc_attr($specs['altscreen'] ) . '-up-' . esc_attr($specs['altcolumns']) .'">' . do_shortcode ( $content ) . '</div>';
		}

		add_shortcode ('blockgrid', 'frn_foundation_blockgridul' );
	///BLOCKGRID

	// BLOCKGRID LIST ITEM
		function frn_foundation_blockgridli ( $atts, $content = null ) {
			return '<div class="sl_column column column-block">' .  do_shortcode ( $content ) . '</div>';
		}

		add_shortcode ('item', 'frn_foundation_blockgridli' );
	///BLOCKGRID LIST ITEM

	// BUTTON
		function frn_foundation_buttons ( $atts, $content = null ) {
			$specs = shortcode_atts( array(
				'url'	=> '#',
				'style'	=> '', 
				'target'	=> '_self'
				), $atts );
			if(in_array('read-next', $specs, true)){
				return '<div class="sl_read-next--button"><h4>Read This Next:</h4><a href="' . esc_attr($specs['url'] ) . '" class="sl_read-next--link sl_button button secondary ' . esc_attr($specs['style'] ) . '" target="'. esc_attr($specs['target'] ) .'">' . $content . '</a></div>';
			} else {
			return '<a href="' . esc_attr($specs['url'] ) . '" class="sl_button button ' . esc_attr($specs['style'] ) . '" target="'. esc_attr($specs['target'] ) .'">' . $content . '</a>';
			};
		}

		add_shortcode ('button', 'frn_foundation_buttons' );
	///BUTTON

	//READ NEXT
		function frn_read_next ( $atts, $content = null ) {
			$specs = shortcode_atts( array(
				'url'	=> '#',
				'title' => '',
				'style'	=> '', 
				'target'	=> '_self'
				), $atts );

				return '<div class="sl_read-next ' . esc_attr($specs['style'] ) . ' "><span>&gt;&gt;&gt; Read This Next:</span> <a class="sl_read-next--link" title="' . esc_attr($specs['title'] ) . '" href="' . esc_attr($specs['url'] ) . '" target="'. esc_attr($specs['target'] ) .'">' . $content . '</a></div>';
		}

		add_shortcode ('read-next', 'frn_read_next' );
	//READ NEXT

	// FLEXVIDEO
		function frn_foundation_flexvideo ( $atts, $content = null ) {
			return '<div class="flex-video">' . $content . '</div>';
		}
		add_shortcode ('flexvideo', 'frn_foundation_flexvideo' );
	///FLEXVIDEO

	// TABS
		function frn_foundation_tabs( $atts, $content = null ) {
			$specs = shortcode_atts( array(
				'style'		=> ''
			), $atts );
			static $i = 0;
			$i++;
			
			return '<ul class="sl_tabs tabs ' . esc_attr($specs['style'] ) . '" data-tabs id="example-tabs' . $i . '">' . do_shortcode ( $content ) . '</ul>';
		}

		add_shortcode ('tabs', 'frn_foundation_tabs' );
	///TABS

	// TAB TITLE
		function frn_foundation_tabs_title ( $atts, $content = null ) {

		    $specs = shortcode_atts( array(
		        'class'     => ''
		    ), $atts );

		    static $i = 0;
		    if ( $i == 0 ) {
		        $class = 'is-active';
		    } else {
		        $class = NULL;
		    }
		    $i++;
		    $value = '<li class="sl_tabs-title tabs-title ' . $class . ' '.esc_attr($specs['class'] ).'"><a href="#tabpanel' . $i . '">' .
		    $content . '</a></li>';

		    return $value;
		}

		add_shortcode ('tab-title', 'frn_foundation_tabs_title' );
	///TAB TITLE

	// TAB CONTENT
		function frn_foundation_tabs_content( $atts, $content = null ) {
			$specs = shortcode_atts( array(
				'style'		=> ''
			), $atts );
			static $i = 0;
			$i++;
			return '<div class="sl_tabs-content tabs-content ' . esc_attr($specs['style'] ) . '" data-tabs-content="example-tabs' . $i . '">' . do_shortcode ( $content ) . '</div>';
		}
		add_shortcode ('tab-content', 'frn_foundation_tabs_content' );
	///TAB CONTENT

	// TAB PANEL
		function frn_foundation_tabs_panel ($atts, $content = null ) {
			static $i = 0;
			if ( $i == 0 ) {
				$class = 'is-active';
			} else {
				$class = NULL;
			}
			$i++;
			return '<div class="sl_tabs-panel tabs-panel ' . $class .'" id="tabpanel' . $i . '">' . do_shortcode( $content ) . '</div>';
		}
		add_shortcode ('tab-panel', 'frn_foundation_tabs_panel' );
	///TAB PANEL

	// ACCORDION
		function frn_foundation_accordion( $atts, $content = null ) {
			$specs = shortcode_atts( array(
				'style'		=> ''
			), $atts );
			return '<ul class="sl_accordion accordion ' . esc_attr($specs['style'] ) . '" data-accordion data-multi-expand="true" data-allow-all-closed="true">' . do_shortcode ( $content ) . '</ul>';
		}
		add_shortcode ('accordion', 'frn_foundation_accordion' );
	///ACCORDION

	// ACCORDION ITEM
		function frn_foundation_tabs_accordion_item ( $atts, $content = null ) {
			$specs = shortcode_atts( array(
				'title'		=> ''
				), $atts );
			static $i = 0;
		/*	if ( $i == 0 ) {
				$class = 'active';
			} else {
				$class = NULL;
			}*/
			$i++;
			$value = '<li class="sl_accordion-item accordion-item" data-accordion-item><a href="#" class="sl_accordion-title accordion-title">' . esc_attr($specs['title'] ) . '</a><div id="panel' . $i . '" class="sl_accordion-content accordion-content" data-tab-content>' . do_shortcode( $content ) . '</div></li>';

			return $value;
		}

		add_shortcode ('accordion-item', 'frn_foundation_tabs_accordion_item' );
	///ACCORDION ITEM



///THESE ARE SOME SHORTCODES THAT ARE INDEPENDENT OF THE FOUNDATION FRAMEWORK

	// DIVIDER
		function line_divider() {

			return '<div class="sl_divider divider clearfix"></div>';

		}
		add_shortcode( 'divider', 'line_divider' );
	///DIVIDER

	// CALLOUT
		function callout_box ( $atts, $content = null ) {
			$specs = shortcode_atts( array(
				'style'		=> '',
				), $atts );
			return '<div class="sl_callout callout ' . esc_attr($specs['style'] ) . '">' . do_shortcode ( $content ) . '</div>';
		}
		add_shortcode ('callout', 'callout_box' );
	///CALLOUT

	// CALLOUT
		function callout_card ( $atts, $content = null ) {
			$specs = shortcode_atts( array(
				'style'		=> '',
				'img'		=> ''
				), $atts );
			return '<div class="sl_callout-card callout-card ' . esc_attr($specs['style'] ) . '"><div class="card-media" style="background-image: url('. esc_attr($specs['img'] ) .'"></div><div class="card-content">' . do_shortcode ( $content ) . '</div></div>';
		}
		add_shortcode ('callout-card', 'callout_card' );
	///CALLOUT

	// LARGE TEXT
		function large_text ( $atts, $content = null ) {
			return '<div class="sl_callout-text callout-text">' . do_shortcode ( $content ) . '</div>';
		}
		add_shortcode ('large_text', 'large_text' );
	///LARGE TEXT

	// CALLOUT EQUALIZER
		function callout_box_equalizer ( $atts, $content = null ) {
			return '<div class="sl_callout callout equalizer" data-equalizer-watch>' . do_shortcode ( $content ) . '</div>';
		}
		add_shortcode ('callout_equalizer', 'callout_box_equalizer' );
	///CALLOUT EQUALIZER

	// MAP
		function map_container ( $atts, $content = null ) {
			return '<div class="map-container">' . $content . '</div>';
		}
		add_shortcode ('map', 'map_container' );
	///MAP

	// CONTACT BOX
		function contact_box_section ( $atts, $content = null ) {
			return '<div class="sl_callout callout equalizer contact-box" data-equalizer-watch>' . do_shortcode ( $content ) . '</div>';
		}
		add_shortcode ('contact_box', 'contact_box_section' );
	///CONTACT BOX

	// CONTAINER
		function div_container ( $atts, $content = null ) {
		    $specs = shortcode_atts( array(
		        'class'     => '',
		        ), $atts );
		    return '<div class="sl_container container ' . esc_attr($specs['class']) . ' ">' .  do_shortcode ( $content ) . '</div>';
		}

		add_shortcode ('container', 'div_container' );
	///CONTAINER

	// EMAIL
	function sl_email () {

		ob_start ();
		?><div class="sl_card sl_card--email">
		<!-- Begin Constant Contact Inline Form Code -->
		<div class="ctct-inline-form" data-form-id="d11806fd-808a-4775-b9de-feb1aac81412"></div>
		<!-- End Constant Contact Inline Form Code -->
	    </div><?php

		$content = ob_get_clean();
		
		return do_shortcode($content);


	}
	add_shortcode( 'email', 'sl_email' );
	//END EMAIL

	// CAROUSEL
	function carousel_gallery ( $atts, $content = null ) {
		$specs = shortcode_atts( array(
			'class'     => '',
			), $atts );
		return '[shortcode_unautop]<div class="sl_carousel__slider">
					'. do_shortcode($content) .'
				</div><!--/.sl_carousel__slider-->[/shortcode_unautop]';
	}

	add_shortcode ('carousel', 'carousel_gallery' );
	// END CAROUSEL

	//FULL WIDTH
	function full_width_function( $atts, $content = null ){
		$specs = shortcode_atts( array(
			'style'		=> '',
				), $atts );
		if(get_post_type($post) == 'page') {
				$html = '
						</div>
					</div>
					<div class="sl_full_width sl_callout ' . esc_attr($specs['style'] ) . '">
						<div class="row">
							<div class="small-12 columns">
								'. do_shortcode($content) .'
							</div>
						</div>
					</div>
					<div class="row">
						<div class="medium-12 columns clearfix tb-pad-30" role="main">';
		} else {
			$html = '
					<div class="sl_full_width sl_callout ' . esc_attr($specs['style'] ) . '">
						<div class="row">
							<div class="small-12 columns">
								'. do_shortcode($content) .'
							</div>
						</div>
					</div>';
		}
		return $html;
	}
	add_shortcode( 'full_width', 'full_width_function' );
	//END FULL WIDTH



//disables wp texturize on registered shortcodes
function frn_shortcode_exclude( $shortcodes ) {
    $shortcodes[] = 'row';
    $shortcodes[] = 'column';
    $shortcodes[] = 'blockgrid';
    $shortcodes[] = 'item';
    $shortcodes[] = 'button';
    $shortcodes[] = 'flexvideo';
    $shortcodes[] = 'tabs';
    $shortcodes[] = 'tab-title';
    $shortcodes[] = 'tab-content';
    $shortcodes[] = 'tab-panel';
    $shortcodes[] = 'accordion';
    $shortcodes[] = 'accordion-item';
    $shortcodes[] = 'modal';
	$shortcodes[] = 'divider';
	$shortcodes[] = 'map';
	$shortcodes[] = 'contact_box';
	$shortcodes[] = 'facility_photo_slider';
	$shortcodes[] = 'frn_privacy_url';
    $shortcodes[] = 'frn_footer';
    $shortcodes[] = 'lhn_inpage';
    $shortcodes[] = 'frn_phone';
    $shortcodes[] = 'frn_social';
    $shortcodes[] = 'frn_boxes';
    return $shortcodes;
}

add_filter ('no_texturize_shortcodes', 'frn_shortcode_exclude' );

// This function reformats autop inside shortcodes. To turn off auto p inside shortcode, wrap shortcode in [shortcode_unautop]

// Example: return '[shortcode_unautop]'. do_shortcode($content) .'[/shortcode_unautop]';

function reformat_auto_p_tags($content) {
    $new_content = '';
    $pattern_full = '{(\[shortcode_unautop\].*?\[/shortcode_unautop\])}is';
    $pattern_contents = '{\[shortcode_unautop\](.*?)\[/shortcode_unautop\]}is';
    $pieces = preg_split($pattern_full, $content, -1, PREG_SPLIT_DELIM_CAPTURE);
    foreach ($pieces as $piece) {
        if (preg_match($pattern_contents, $piece, $matches)) {
            $new_content .= $matches[1];
        } else {
            $new_content .= wptexturize(wpautop($piece));
        }
    }

    return $new_content;
}

remove_filter('the_content', 'wpautop');
remove_filter('the_content', 'wptexturize');

add_filter('the_content', 'reformat_auto_p_tags', 99);
add_filter('widget_text', 'reformat_auto_p_tags', 99);

// // Allows shortcode use inside ACF fields
function my_acf_add_local_field_groups() {
    remove_filter('acf_the_content', 'wpautop' );
}
add_action('acf/init', 'my_acf_add_local_field_groups');


?>