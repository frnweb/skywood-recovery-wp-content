<?php
/*
Template Name: Frankenstein's Home Page
*/
?>
<?php get_header(); ?>
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			<?php 
				$post_thumbnail_id = get_post_thumbnail_id();
				$featured_src = wp_get_attachment_image_src( $post_thumbnail_id, 'full' );
				$featured_mobile_src = get_field( 'mobile_featured_image');
			?>
			<div class="frank-sections">
				<div class="frank-hero module">
					<div class="frank-hero__background">
						<div class="bg-1"></div>
						<div class="bg-2" style="background-image: url('<?php echo get_template_directory_uri(); ?>/images/home-banner-image.jpg');"></div>
					</div>
					<div class="frank-row">
						
						<div class="card card--0">
							<div class="symbol-grid">
								<div class="symbol">
									<img class="symbol-img" src="<?php echo get_template_directory_uri(); ?>/images/jointcommission.png" />
								</div>
								
							</div><!-- /.symbol-grid -->
							<h4>NATIONALLY ACCREDITED ADDICTION TREATMENT</h4>
							<h1>300 Acres Underneath Serene Augusta Skies</h1>
						</div><!-- /.card--0 -->

						<div class="card card--callout card--1">
							<h4 class="callout-subhead">Have questions about treatment? We are here to help!</h4>
							<?php echo do_shortcode('[frn_phone class="button button--hero" action="Phone Clicks in Hero Section"]');?>
						</div><!-- /.card--1 -->

					</div><!-- /.frank-row -->
				</div><!-- /.frank-hero -->

				<div class="frank-treatment module">
					<div class="frank-intro">
						<h1 class="intro-header">Inpatient, outpatient, and detox treatment for…</h1>
					</div>
					<div class="frank-row">
						<div class="card card--0">
							<h2>Substance Abuse</h2>
							<ul>
								<li>Alcohol</li>
								<li>Opiates</li>
								<li>Amphetamines</li>
								<li>Cocaine</li>
								<li>Prescription Pills</li>
								<li>Hallucinogens</li>
								<li>Benzodiazepines</li>
							</ul>
						</div>

						<div class="card card--1">
							<h2>Mental Health</h2>
							<ul>
								<li>Anxiety</li>
								<li>Depression</li>
								<li>Co-Occuring Disorders</li>
								<li>Borderline Personality</li>
								<li>ADD/ADHD</li>
								<li>Bipolar</li>
								<li>Trauma</li>
							</ul>
						</div>
					</div>
				</div><!-- /.frank-treatment -->
				
				<div class="frank-insurance module">

					<div class="sl_inner">
						<h2 style="text-align: center;">A lifetime of recovery is just one phone call away.</h2>
						<p style="text-align: center;">Take a step towards a happier, healthier life.</p>

						<div style="max-width: 600px; margin: 0 auto;">
							<div class="flex-video widescreen">
								<iframe width="100%" height="100%" src="https://www.youtube.com/embed/aIKxULuzvw8" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
							</div><!-- /.flex-video.widescreen -->
						</div><!-- div -->

					</div><!-- /.sl_inner -->

					<div class="frank-row">

						<div class="card card--0">
							<div class="insurance-grid">
								<!-- <div class="cell">
									<img class="insurance-logo" src="<?php echo get_template_directory_uri(); ?>/images/insurance/priority-health.png" />
								</div> -->

								<div class="cell">
									<img class="insurance-logo" src="<?php echo get_template_directory_uri(); ?>/images/insurance/cigna.png" />
								</div>

								<div class="cell">
									<img class="insurance-logo" src="<?php echo get_template_directory_uri(); ?>/images/insurance/magellan.png" />
								</div>

								<div class="cell">
									<img class="insurance-logo" src="<?php echo get_template_directory_uri(); ?>/images/insurance/aetna.png" />
								</div>

								<div class="cell">
									<img class="insurance-logo" src="<?php echo get_template_directory_uri(); ?>/images/insurance/humanalogo.png" />
								</div>

								<div class="cell">
									<img class="insurance-logo" src="<?php echo get_template_directory_uri(); ?>/images/insurance/valueoptions.png" />
								</div>

								<div class="cell">
									<img class="insurance-logo" src="<?php echo get_template_directory_uri(); ?>/images/insurance/beaconhealth.png" />
								</div>

								<div class="cell">
									<img class="insurance-logo" src="<?php echo get_template_directory_uri(); ?>/images/insurance/coresource.png" />
								</div>
							</div>
						</div>

						<div class="card card--1">
							<h2>Insurance Providers</h2>
							<p>We accept most major insurances and many more than are pictured here. Our admissions team will help determine what is covered and help you calculate any out-of-pocket costs.</p>
							<span class="cta">Free insurance verification:</span>
							<?php echo do_shortcode('[frn_phone class="button button--insurance" action="Phone Clicks in Hero Section"]');?>
						</div>

						<div class="card card--2">
							<h2 class="reputation-header"> Recent Reviews </h2>
							<a class="reputation-widget" target="_blank" href="https://widgets.reputation.com/widgets/6109837d8dd068152fa2c831/run?tk=52d91720521" data-tk="52d91720521" data-widget-id="6109837d8dd068152fa2c831" env="" region="us">Reputation Reviews</a>
<script>!function(d,s,c){var js,fjs=d.getElementsByTagName(s)[0];js=d.createElement(s);js.className=c;js.src="https://widgets.reputation.com/src/client/widgets/widgets.js?v=1592944030435";fjs.parentNode.insertBefore(js,fjs);}(document,"script","reputation-wjs");</script>
						</div>

					</div>
				</div><!-- /.frank-insurance -->
			</div><!-- /.frank-sections -->

			<!-- Gallery Slider -->
        	<?php get_template_part( 'parts/content', 'gallery' ); ?>
        	<!-- End PGallery Slider -->
				
			<!-- home-first-section -->	
				<div id="home-first-section">
					<div id="swirly"></div>
					<div id="gray-content">
						<div class="row">
							<div class="large-10 large-centered columns text-center">
								<h2 class="section-title"><?php echo get_field('gray_title'); ?></h2>
								<h3><?php echo get_field('gray_quote'); ?></h3>
								<p><?php echo get_field('gray_speaker'); ?></p>
							</div>
						</div>
						<div class="row">
							<div class="large-6 large-centered columns text-center">
								<?php echo get_field('gray_content'); ?>
								<a href="<?php echo get_field('gray_button_url'); ?>" class="button secondary large"><?php echo get_field('gray_button_text'); ?></a>
							</div>
						</div>
					</div>
				</div>
			
			<!-- assess-contact -->
				<div id="assess-contact" class="tb-pad-90">
					<div class="row">
						<div class="large-12 columns">
							<h2 class="section-title">Take an assessment</h2>
							<p id="assess-subtitle" class="text-center primary-color">AND SEND RESULTS TO AN ADMISSIONS COORDINATOR</p>
						</div>
					</div>
					<div class="row ">
						<div class="medium-6 columns">
							<div id="assessment-shell">

							</div>
						</div>
						<div class="medium-6 columns" id="home-contact-form">
							<?php 
								$contact_form = get_field('contact_form_7_shortcode');
								echo do_shortcode( $contact_form );
							?>
						</div>
					</div>
				</div>
					
					<?php 
					
					//video testimonialtype
					$args = array(
						'post_type'=>'testimonial',
						'orderby'=>'menu_order',
						'order'=>'asc'
					);
					$testimonials = new WP_Query( $args );
					if($testimonials->have_posts()) : 
						?>
						
						<div id="video-testimonials">
							<div class="row tb-pad-60">
								<div class="small-12 columns">
									<h2 class="section-title">
										Watch Video
									</h2>
									<div class="row text-center">
										<div class="large-8 medium-10 medium-centered columns">
											<h3><?php echo get_field('video_quote'); ?></h3>
											<p><?php echo get_field('video_quote_speaker'); ?></p>
										</div>
									</div>
									<div class="row">
										<div class="large-6 medium-8 medium-centered columns">
											<div class="row tb-pad-20 small-up-1">
												<?php 
												while($testimonials->have_posts()) : 
													$testimonials->the_post();	
													$title = get_the_title();
													$feat_image = wp_get_attachment_url( get_post_thumbnail_id() );
													$video = get_field('video_url');
													?>
													<div class="column">
														<a class="fancybox-media" href="<?php echo $video ?>">
															<span class="vid-test-thumb-box" style="background: url('<?php echo $feat_image ?>') no-repeat center center / cover">
																<span class="vid-play-icon"></span>
															</span>
														</a>
													</div>
													<?php
													
													
												endwhile;
												?>
											</div><!-- end row -->
										</div>
									</div>
								</div>
							</div>
						</div>
						
						<?php 
					endif;
					
					?>
					<?php 
					//insurance content type
					$args = array(
						'post_type'=>'hp_treatment',
						'orderby'=>'menu_order',
						'order'=>'asc'
					);
					$treatments = new WP_Query( $args );
					if($treatments->have_posts()) : 
						?>
						<div id="treatment-section">
							<div class=" tb-pad-60" style="padding-bottom: 0;">
								<div class="col-sm-12">
									<div id="treatment-section-leadin">
										<h2 class="section-title">
											OUR APPROACH TO RECOVERY
										</h2>
										<?php 
											if(get_field('treatment_subheading')){
												the_field('treatment_subheading');
											}
										?>
									</div>
									<?php 
									$treatment_count = 0;
									while($treatments->have_posts()) : 
										$treatment_count++;
										$treatments->the_post();	
										$title = get_the_title();
										$feat_image = wp_get_attachment_url( get_post_thumbnail_id() );
										?>
										<div class="row expanded collapse" data-equalizer data-equalize-on="medium">
											<div class="medium-6 columns <?php echo ($treatment_count %2 == 0) ? 'float-right' : '' ; ?>" >
												<div class="treatment-feature-image-box" data-equalizer-watch>
													<div class="treatment-feature-image" style="background: url('<?php echo $feat_image ?>') no-repeat center center / cover; transition: all 2.5s ease; -moz-transition: all 2.5s ease; -ms-transition: all 2.5s ease; -webkit-transition: all 2.5s ease; -o-transition: all 2.5s ease;" data-equalizer-watch>
												</div>
												</div>
											</div>
											<div class="medium-6 columns <?php echo ($treatment_count %2 == 0) ? 'float-left text-right' : '' ; ?>" data-equalizer-watch>
												<div class="treatment-pad">
													<div class="large-8 columns <?php echo ($treatment_count %2 == 0) ? 'float-right' : '' ; ?>">
														<div class="title">
															<h3><?php echo $title; ?></h3>
														</div>
														<div class="content">
															<?php the_content() ?>
														</div>
														<div class="button-shell">
															<?php 
																$page_url = get_field('page_url');
															//var_dump( $obj );
																
																?>
							
															<a  class="button" href="<?php echo $page_url ?>">
																Learn More
															</a>
																<?php 
																
															?>
														</div>
													</div>
													<div class="clearfix"></div>
												</div>
											</div>
										<?php
										?>
										</div><!-- end row -->
									<?php
									endwhile;
									?>
								</div>
							</div>
							
						</div>
						<?php 
					endif;

					
					//insurance content type
					$args = array(
						'post_type'=>'insurance',
						'orderby'=>'menu_order',
						'order'=>'asc'
					);
					$insurances = new WP_Query( $args );
					if($insurances->have_posts()) : 
					?>
						<div id="insurance-section" class="tb-pad-90">
							<div class="row">
								<div class="small-12 columns">
									<h2 class="primary-color text-center">
										WE ACCEPT THESE INSURANCES AND MORE
									</h2>
								</div>
							</div>
							<div class="tb-pad-20">
								<div class="row small-up-2 medium-up-2 large-up-4">
									<?php 
									while($insurances->have_posts()) : 
										$insurances->the_post();	
										$title = get_the_title();
										$feat_image = wp_get_attachment_url( get_post_thumbnail_id() );
										?>
										<div class="column" style="padding-bottom: 20px;">
											<img class="insurance-logo" alt="<?php echo $title; ?>" src="<?php echo $feat_image ?>"/>
										</div>
										<?php
										
										
									endwhile;
									?>
								</div><!-- end row -->
							</div>
							<div class="row">
								<div class="large-12 columns text-center"><a href="<?php echo site_url(); ?>/admissions/insurance-coverage/" class="button">Learn More</a></div>
							</div> 
						</div>
					<?php endif; ?>
					
			<?php endwhile; ?>
		<?php endif; ?>

<?php get_footer(); ?>