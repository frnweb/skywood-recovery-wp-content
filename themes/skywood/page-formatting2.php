<?php
/*
Template Name: Formatting2
*/
?>
<?php get_header(); ?>
			
			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
				<?php 
					$src = get_page_banner();
				?>
				<header>
					<div class="page-header interior-top-banner blog-stretchy-wrapper" style="background-image: url('<?php echo $src; ?>');">
						<div>
							<div class="row interior-top-text-box">
								<div class="small-12 columns">
									<div style="display: table; width: 100%;">
										<div style="display: table-cell; vertical-align: middle;">
											<h1 class="page-title" style="color: #fff;" itemprop="headline"><?php the_title(); ?></h1>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</header> <!-- end article header -->
				
					<div id="main" class="" role="main">

						
						
						<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">				
							
							
						
							<section class="post_content clearfix" itemprop="articleBody">
										
											<div class="clearfix row">
												<div id="main" class="medium-12 columns clearfix tb-pad-30" role="main">
													<?php the_post_thumbnail( 'full' ); ?>
													<?php /*echo '<h1>Content part '.$content_count.'</h1>';*/ ?>
													<?php the_content(); ?>
													<?php wp_link_pages(); ?>
													<?php 
														// only show edit button if user has permission to edit posts
														if( $user_level > 0 ) { 
													?>
														<a href="<?php echo get_edit_post_link(); ?>" class="btn btn-success edit-post"><i class="icon-pencil icon-white"></i> <?php _e("Edit post","wpbootstrap"); ?></a>
													<?php } ?>
												</div> <!-- end #main -->
											</div>
						
							</section> <!-- end article section -->

							
							<footer>
				
								
								
							</footer> <!-- end article footer -->
						
						</article> <!-- end article -->
						
						<?php echo get_resources_block(); ?>
						
						
				
					</div> <!-- end #main -->
		 
					
		 
			
			<?php endwhile; ?>		
					
			<?php endif; ?>

<?php get_footer(); ?>