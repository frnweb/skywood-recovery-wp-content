<?php get_header(); ?>
			
			<div class="clearfix row" data-equalizer="outer" data-equalize-on="large">
			
				<?php get_sidebar(); // sidebar 1 ?>
				
				<div id="main" class="large-9 columns clearfix float-left" role="main" data-equalizer-watch="outer">
					<?php $post_count = 0; ?>
					<div class="page-header">
					<?php if (is_category()) { ?>
						<h1 class="archive_title h2">
							<span><?php _e("Categorized:", "wpbootstrap"); ?></span> <?php single_cat_title(); ?>
						</h1>
					<?php } elseif (is_tag()) { ?> 
						<h1 class="archive_title h2">
							<span><?php _e("Tagged:", "wpbootstrap"); ?></span> <?php single_tag_title(); ?>
						</h1>
					<?php } elseif (is_author()) { ?>
						<h1 class="archive_title h2">
							<span><?php _e("By:", "wpbootstrap"); ?></span> <?php get_the_author_meta('display_name'); ?>
						</h1>
					<?php } elseif (is_day()) { ?>
						<h1 class="archive_title h2">
							<span><?php _e("Daily Archives:", "wpbootstrap"); ?></span> <?php the_time('l, F j, Y'); ?>
						</h1>
					<?php } elseif (is_month()) { ?>
						 <h1 class="archive_title h2">
							<span><?php _e("Monthly Archives:", "wpbootstrap"); ?></span> <?php the_time('F Y'); ?>
						 </h1>
					<?php } elseif (is_year()) { ?>
						 <h1 class="archive_title h2">
							<span><?php _e("Yearly Archives:", "wpbootstrap"); ?></span> <?php the_time('Y'); ?>
						 </h1>
					<?php } ?>
					</div>
					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
						<?php 
							$post_count++;
							$bsrc = get_field('thumbnail_image', $post->ID);
							$banner_src = (!empty($bsrc)) ? $bsrc : get_field('default_resource_image', 'option');
						?>
						<div class="row" data-equalizer="inner" data-equalize-on="medium">
							<div class="medium-6 columns <?php echo ($post_count %2 == 0) ? 'float-right' : '' ; ?>" style="background: url('<?php echo $banner_src['url']; ?>') no-repeat center center / cover;" data-equalizer-watch="inner">
								
							</div>
							<div class="medium-6 columns <?php echo ($post_count %2 == 0) ? 'float-left text-right' : '' ; ?>" style="<?php echo ($post_count %2 != 0) ? 'padding-right: 20px;' : '' ; ?>" data-equalizer-watch="inner">
								<div class="tb-pad-40">
									<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article">
									
										<header>
											<div class="page-header"><h2 class=""><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2></div>
										</header> <!-- end article header -->
									
										<section class="post_content clearfix">
											<?php the_excerpt(); ?>
										</section> <!-- end article section -->
										
										<footer>
											<p class="tags hide"><?php the_tags('<span class="tags-title">' . __("Tags","wpbootstrap") . ':</span> ', ' ', ''); ?></p>
										</footer> <!-- end article footer -->
									
									</article> <!-- end article -->
								</div>
							</div>
						</div>
							
					
					<?php endwhile; ?>	
					
					<?php if (function_exists('wp_bootstrap_page_navi')) { // if expirimental feature is active ?>
						
						<?php wp_bootstrap_page_navi(); // use the page navi function ?>
						
					<?php } else { // if it is disabled, display regular wp prev & next links ?>
						<nav class="wp-prev-next">
							<ul class="pager">
								<li class="previous"><?php next_posts_link(_e('&laquo; Older Entries', "wpbootstrap")) ?></li>
								<li class="next"><?php previous_posts_link(_e('Newer Entries &raquo;', "wpbootstrap")) ?></li>
							</ul>
						</nav>
					<?php } ?>		
					
					<?php else : ?>
					
					<article id="post-not-found">
					    <header>
					    	<h1><?php _e("Not Found", "wpbootstrap"); ?></h1>
					    </header>
					    <section class="post_content">
					    	<p><?php _e("Sorry, but the requested resource was not found on this site.", "wpbootstrap"); ?></p>
					    </section>
					    <footer>
					    </footer>
					</article>
					
					<?php endif; ?>
			
				</div> <!-- end #main -->
    
				
    
			</div> <!-- end #content -->

<?php get_footer(); ?>