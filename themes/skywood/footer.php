				<div id="call-us">

					<div class="row tb-pad-60">

						<div class="large-12 columns text-center">

							<img src="<?php echo get_template_directory_uri()  ?>/images/icon.phone.png" alt="" style="width: 40px; margin-bottom: 30px;">

							<h2 id="footer-phone">Call us now at <?php echo do_shortcode('[frn_phone]');?></h2>

							<p id="call-us-or-find">Or find us at</p>

							<div id="call-us-address"><a href="https://goo.gl/maps/4PT36Yv64NScum3FA" target="_blank"><?php echo get_field('company_address', 'option'); ?></a></div>

						</div>

					</div>

				</div>

				<!-- Email Form -->
		        <div class="sl_footer__email">
		            <div class="sl_inner">
		                <?php echo do_shortcode('[email]'); ?>
		            </div>
		        </div>

			</div><!-- #content -->

		</div><!-- nav sticky -->

		<footer class="tb-pad-60">

			<div class="row">

				<div class="large-12 columns">

					<div class="text-center">

						<div>

							<?php wp_bootstrap_footer_links(); // Adjust using Menus in Wordpress Admin ?>

						</div>

						<div id="mobilesocial">

							<ul class="menu">
							

								<li><a href="<?php the_field('twitter_url', 'option') ?>" class="social twitter"></a></li>

								<li><a href="<?php the_field('facebook_url', 'option') ?>" class="social facebook"></a></li>

								<li><a href="<?php the_field('linkedin_url', 'option') ?>" class="social linkedin"></a></li>

							</ul>

						</div>

<div id="mobilebbb" style="padding: 1em;"><ul class="menu">

<li><a style="padding: 0;" href="http://www.jointcommission.org"><img style="padding: 0px; border: none; width: 100px;" src="/wp-content/uploads/JointCommission.gif"></a>
</li>
<li><script src="https://static.legitscript.com/seals/3417927.js"></script>
</li>
<li><a href="https://www.naatp.org/resources/addiction-industry-directory/8141/skywood-recovery" target="_blank"><img src="https://www.naatp.org//civicrm/file?reset=1&id=4087&eid=201&fcs=1f26e81149c63e3ea4639b16e1ccf9e6a66df075bd944bb7ddb8b7024507c576_1585039261_87600"></a>
</li>

</ul>
<p>Skywood Recovery is dedicated to providing the highest level of quality care to our patients. The Joint Commission’s gold seal of approval on our website shows that we have demonstrated compliance to the most stringent standards of performance, and we take pride in our accreditation. The Joint Commission standards deal with organization quality, safety-of-care issues and the safety of the environment in which care is provided. If you have concerns about your care, we would like to hear from you. Please contact us at <?php echo do_shortcode('[frn_phone number="678-251-3100"]'); ?>. If you do not feel that your concerns have been addressed adequately, you may contact The Joint Commission at: Division of Accreditation Operations, Office of Quality Monitoring, The Joint Commission, One Renaissance Boulevard, Oakbrook Terrace, IL 60181, Telephone: <?php echo do_shortcode('[frn_phone number="800-994-6610"]'); ?></p>
</div>

						

						<?php echo do_shortcode('[frn_footer]');?>
					</div>

				</div>

			</div>

		</footer>

		<!--[if lt IE 7 ]>

  			<script src="//ajax.googleapis.com/ajax/libs/chrome-frame/1.0.3/CFInstall.min.js"></script>

  			<script>window.attachEvent('onload',function(){CFInstall.check({mode:'overlay'})})</script>

		<![endif]-->

		

		<?php wp_footer(); // js scripts are inserted using this function ?>



		<script src="<?php echo get_template_directory_uri()  ?>/js/app.js?v=1.02"></script>

		<?php if ( is_front_page() ): ?>

			<script src="<?php echo get_template_directory_uri()  ?>/library/js/fancybox/source/jquery.fancybox.js"></script>

			<link type="text/css" href="<?php echo get_template_directory_uri()  ?>/library/js/fancybox/source/jquery.fancybox.css" rel="stylesheet"></link>

			<script src="<?php echo get_template_directory_uri()  ?>/library/js/fancybox/source/helpers/jquery.fancybox-media.js"></script>

			<script src="<?php echo get_template_directory_uri()  ?>/library/js/skywood-homepage.js"></script>



		<?php endif; ?>

	<?php if(  is_front_page()):?>

<script id="assessment-template" type="text/x-handlebars-template">

	<div id="assessment" class="">

		<div class="questions clearfix">

		{{#each questions }}

			<div class="question question_{{ @index }}" data-index="{{ @index }}">

				<div class="title">

					{{{ this.title.rendered }}}

				</div>

				<div class="buttons">

					<div class="row tb-pad-20">

						<div class="small-6 medium-5 columns">

							<button type="button" class="button large expanded  answer" data-answer="yes" data-index="{{ @index }}">

								Yes

							</button>

						</div>

						<div class="small-6 medium-5 columns">

							<button type="button" class="button large expanded  answer" data-answer="no" data-index="{{ @index }}">

								No

							</button>

						</div>

						<div class="medium-2 columns show-for-medium"></div>

					</div>

				</div>

				<div class="message">

				</div>

				<div class="nav-buttons hide">

					{{#if @first }}

						

					{{else}}

						<button type="button" class="button nav" data-action="back" data-index="{{ @index }}">

							Back

						</button>						

					{{/if}}

					{{#if @last }}

						

					{{else}}

						<button type="button" class="button nav" data-action="next" data-index="{{ @index }}">

							Next

						</button>	

					{{/if}}

				</div>

				<div class="clearfix"></div>

			</div>



		{{/each}}

		</div><!-- questions -->

		<div style="clear:both"></div>

		<div class="row" id="assess-progress-bar">

			<div class="medium-4 large-3 columns show-for-medium">

				<p style="margin-bottom: 0; line-height: .8;">Your Progress</p>

			</div>

			<div class="medium-8 large-9 columns">

				<div class="secondary progress">

					<div class="progress-meter"></div>

				</div>

			</div>

			

		</div>

		<div class="clearfix"></div>

		<div id="assess-arrow-shell" class="show-for-medium">

			<div id="assess-arrow-outer"></div>

			<div id="assess-arrow-inner"></div>

		</div>

	</div>

</script>

<script id="question-template" type="text/x-handlebars-template">

	<div class="question ">

			<h3>

			

			</h3>

	</div>

</script>

<script id="result-template" type="text/x-handlebars-template">

	<div class="question" data-index="{{ total }}">

		<div class="title">Results</div>

		<div class="">

			<p>{{{message}}}</p>

			<p id="assess-phone"><?php echo do_shortcode('[frn_phone]');?></p>

		</div>

	</div>

</script>

		<script src="<?php echo get_template_directory_uri()  ?>/library/js/handlebars.min.js"></script>

		<script src="<?php echo get_template_directory_uri()  ?>/library/js/jquery.cycle2.js"></script>

		<script src="<?php echo get_template_directory_uri()  ?>/library/js/assessments.js"></script>

	<?php endif; ?>

	</body>

</html>