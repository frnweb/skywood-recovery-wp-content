
<?php get_header(); ?>
			
			
			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			
				<?php 
					$src = get_page_banner();
					
				?>
				<script>
					$(document).ready(function(){
						if($(window).width() > 1024) {
							var scrollSpeed = 0.40;
							var lakeOffset = (-1*$('#about-lake-callout').offset().top*scrollSpeed)-$('#large-nav').outerHeight();
							$('#about-lake-callout').css({'background-position':'center top '+lakeOffset+'px'});
							$(window).scroll(function(){
								var lakeCurPos = lakeOffset+($(window).scrollTop()*scrollSpeed);
								$('#about-lake-callout').css({'background-position':'center top '+lakeCurPos+'px'});
							});
						}
					});
				</script>
				<header>
					<div class="page-header interior-top-banner blog-stretchy-wrapper" style="background-image: url('<?php echo $src; ?>');">
						<div>
							<div class="row interior-top-text-box">
								<div class="small-12 columns">
									<div style="display: table; width: 100%;">
										<div style="display: table-cell; vertical-align: middle;">
											<h1 class="page-title" style="color: #fff;" itemprop="headline">About Skywood Recovery</h1>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</header> <!-- end article header -->
			
			

					
				<div id="about-page">
					<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">		
						
					
						<section class="post_content clearfix " itemprop="articleBody">
							<div class="clearfix row tb-pad-40">
			
								<div id="main" class="small-12 columns clearfix" role="main">
									<div class="small-12 large-6 columns" style="float: right; text-align: center;">
										<a href="<?php echo site_url(); ?>/staff-members"><img src="<?php echo get_template_directory_uri()  ?>/images/AdamMarion.png" alt="" class="" style="width: 200px;height: auto;display: inline-block;float: none;margin: 0 auto; margin-right: 5px;"><img src="<?php echo get_template_directory_uri()  ?>/images/LoriRyland.png" alt="" class="" style="width: 200px;height: auto;display: inline-block;float: none;margin: 0 auto; margin-left: 5px;"></a>
										<a href="<?php echo site_url(); ?>/staff-members"><h2 style="margin: 0 auto;">Our Team and Expertise</h2></a>
										<a href="<?php echo site_url(); ?>/staff-members"><div class="more-link button secondary" title="Meet Our Staff" style="margin: 0 auto; display: inline-block;">Meet Our Staff »</div></a>
									</div>
										<p class="lead">Everyone has the ability to recover from addiction and mental health issues.</p>
										<p>At Skywood Recovery Center, a residential treatment facility under the skies of Augusta, Michigan, we can help you rediscover your authentic self in recovery.</p>
										<p>You may feel entirely helpless. You may be completely heartbroken over what addiction has done to you and your family. But here, we firmly believe that everyone has the ability to recover. We believe it because we’ve seen real healing revive the lives and hearts of so many who were once hopeless and heartbroken.</p>
										<p><b>Skywood is not just another rehab.</b> Your loved one may have seen the inside and outside of a handful of treatment centers already. You feel like you’ve had enough of the whole “rehab” thing, but the person you love still needs help.  Phone call after phone call has left you convinced that shopping for treatment is a hassle, and no one can guarantee that they are any better than the rest. What could possibly be different about Skywood? Many programs sound the same, hoping to inspire you with clinical terms like “Dialectical Behavior Therapy,” “EMDR,” “medication-assisted treatment,” and more.</p>
										<p>All those things are well enough, but how can you tell what kind of experience Skywood offers? How can you find out 
								</div>
							</div>
							<div id="about-lake-callout" class="tb-pad-40">
								<div class="row">
									<div class="small-12 columns">
										<h3>“Will it work this time?”</h3>
										<p>Anyone promising a cure for addiction is peddling a scheme. At Skywood, we will not make any false promises or flighty guarantees. Addiction is a chronic disease like cancer. It has no real cure—but it is very treatable.</p>
										<div class="resource-quote">
											<div class="quote">
												<blockquote>
													<p>“When I came here, I wanted help, but I said, ‘I don’t care if I die.’ Now I want to live.”</p>
												</blockquote>
											</div>
											<div class="speaker"><p>A graduate of an FRN treatment program</p></div>
										</div>
										<p>What we do at Skywood is different for several reasons. First, we treat addiction and mental health at the same time in a highly integrated way, looking to restore a person’s physical, emotional, mental and spiritual well-being. Second, we focus on resolving trauma, underlying issues and deep-rooted emotional pain that often causes substance abuse in the first place. Lastly, we emphasize life skills that help each person build a foundation for sustainable, long-term recovery. After all, your recovery is <i>yours, </i>and we will treat you as a partner—not merely a “patient”—in developing your own recovery lifestyle. We will assist you in meeting your personal goals and give you the tools to succeed long-term.</p>
									</div>
								</div>
							</div>
							<div class="row tb-pad-40">
								<div class="small-12 columns">
									
									<p style="font-weight: normal;"><em>So if you want to know if it will work, let us tell you what we’ve seen as a result of our programs:</em></p>
									<div class="row">
										<div class="medium-6 columns">
											<ul>
												<li><b>Deep, transformational life change:</b> Your husband stops stealing money. He starts going work everyday and making new friends at his sober living house. He goes to 12-Step meetings and begins wood-working as a new hobby.</li>
												<li><b>Genuine hope and happiness:</b> Your daughter laughs again. She jokes around like she used to and enjoys meals with the family, tea parties with her daughter and exercise with friends in her yoga class.</li>
												<li><b>Rediscovery of one’s authentic self:</b> Your brother sounds like himself again. He calls you up from time to time to tell you that he loves you. He makes time for his family and he’s regularly cooking healthy meals.</li>
											</ul>
										</div>
										<div class="medium-6 columns">
											<ul>
												<li><b>Healthier relationships:</b> Your nephew doesn’t spend all his time isolating himself anymore. He joins a soccer club and makes friends who don’t do drugs. He has made amends with his father, and now they have lunch every week.</li>
												<li><b>Excitement and fun <i>without substances:</i></b> Your sister is having the time of her life as a runner with friends in recovery. She goes on bike rides and has movie nights with her peers. She finds great restaurants to try out and new hiking trails to explore with you and your family.</li>
											</ul>
										</div>
									</div>
								</div>
							</div>
							<div id="about-gray-callout" class="tb-pad-60">
								<div class="row">
									<div class="medium-4 columns">
									
									</div>
									<div class="medium-8 columns">
										<h2>We’ve seen it happen.</h2>
										<p>None of this occurs overnight, and it takes time, effort and support to face the challenges that arise in recovery. The process is not linear, and it’s often messy, especially at first. We know the obstacles firsthand, and we’re prepared to help you and your loved ones overcome them. Our treatment methods have a deep-seated history in research and quality assurance. All of our experience and expertise fosters real, dynamic healing and our approach to recovery is evidence-based.</p>
									</div>
								</div>
							</div>
							<div class="row tb-pad-40">
								<div class="medium-8 columns">

									<h3>What Really Matters</h3>
									<p>At Skywood, we have the therapeutic methods and the programs that have proven to work. But it isn’t about the programs. <span style="font-weight: normal;"><em>It’s about you and your family.</em></span></p>
									<p>We have unique, comfortable accommodations and special features to our campus, but all of these are a sidenote compared to the experience you or your loved one will have with us. And our compassionate staff will come alongside you each step of the way. They know how scary it can be to move forward into a new future, and they know how the magic of recovery happens. They are ready and willing to show you the way.</p>
									<p>We are challenged to shine brightest in times of adversity. As the earlier quote from poet-philosopher Noah benShea states, “Don’t fear the darkness.” At Skywood, you may be amazed by how your light begins to shine.</p>
									<p>Contact us today to find out what we can do for you and those you love. Stay encouraged—healing is within reach.</p>

								</div> <!-- end #main -->
								
								<div class="medium-4 columns">
									<img src="<?php echo get_template_directory_uri()  ?>/images/about-family-photo.jpg" alt="" style="margin-top: 20px;">
								</div>
					 
							</div> <!-- end #content -->
						</section> <!-- end article section -->
						
						<footer>
			
							<?php the_tags('<p class="tags"><span class="tags-title">' . __("Tags","wpbootstrap") . ':</span> ', ', ', '</p>'); ?>
							
						</footer> <!-- end article footer -->
					
					</article> <!-- end article -->
					
					<?php 

					$resources = get_resources_from_page() ;
					if($resources) {
						
					}
					?>
					
				</div>
			
				
			
			<?php endwhile; ?>		
					
			<?php endif; ?>

<?php get_footer(); ?>