<!doctype html>  

<!--[if IEMobile 7 ]> <html <?php language_attributes(); ?>class="no-js iem7"> <![endif]-->
<!--[if lt IE 7 ]> <html <?php language_attributes(); ?> class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html <?php language_attributes(); ?> class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html <?php language_attributes(); ?> class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html <?php language_attributes(); ?> class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|(gt IEMobile 7)|!(IEMobile)|!(IE)]><!--><html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->
	
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title><?php wp_title( '|', true, 'right' ); ?></title>	
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="cf-2fa-verify" content="25271d6c82c922a">
  		<link rel="apple-touch-icon" sizes="57x57" href="<?php echo get_template_directory_uri()  ?>/images/favicon/apple-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="60x60" href="<?php echo get_template_directory_uri()  ?>/images/favicon/apple-icon-60x60.png">
		<link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_template_directory_uri()  ?>/images/favicon/apple-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="76x76" href="<?php echo get_template_directory_uri()  ?>/images/favicon/apple-icon-76x76.png">
		<link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_template_directory_uri()  ?>/images/favicon/apple-icon-114x114.png">
		<link rel="apple-touch-icon" sizes="120x120" href="<?php echo get_template_directory_uri()  ?>/images/favicon/apple-icon-120x120.png">
		<link rel="apple-touch-icon" sizes="144x144" href="<?php echo get_template_directory_uri()  ?>/images/favicon/apple-icon-144x144.png">
		<link rel="apple-touch-icon" sizes="152x152" href="<?php echo get_template_directory_uri()  ?>/images/favicon/apple-icon-152x152.png">
		<link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri()  ?>/images/favicon/apple-icon-180x180.png">
		<link rel="icon" type="image/png" sizes="192x192"  href="<?php echo get_template_directory_uri()  ?>/images/favicon/android-icon-192x192.png">
		<link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_template_directory_uri()  ?>/images/favicon/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="96x96" href="<?php echo get_template_directory_uri()  ?>/images/favicon/favicon-96x96.png">
		<link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_template_directory_uri()  ?>/images/favicon/favicon-16x16.png">
		<link rel="manifest" href="<?php echo get_template_directory_uri()  ?>/images/favicon/manifest.json">
		<meta name="msapplication-TileColor" content="#ffffff">
		<meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri()  ?>/images/favicon/ms-icon-144x144.png">
		<meta name="theme-color" content="#ffffff">
		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
		<link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,300,300italic,400italic,600,700,600italic,700italic' rel='stylesheet' type='text/css'>

		<!-- wordpress head functions -->
		<?php wp_head(); ?>
		<!-- end of wordpress head -->

		<!-- IE8 fallback moved below head to work properly. Added respond as well. Tested to work. -->
			<!-- media-queries.js (fallback) -->
		<!--[if lt IE 9]>
			<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>			
		<![endif]-->

		<!-- html5.js -->
		<!--[if lt IE 9]>
			<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->	
		
			<!-- respond.js -->
		<!--[if lt IE 9]>
		          <script type='text/javascript' src="http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.js"></script>
		<![endif]-->
		<!-- Google Tag Manager -->
			<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
			new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
			j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
			'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
			})(window,document,'script','dataLayer','GTM-538WG3K');</script>
		<!-- End Google Tag Manager -->		
	</head>
	
	<body <?php body_class(); ?>>
	<!-- Google Tag Manager (noscript) -->
		<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-538WG3K"
		height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->
	<!-- Begin Constant Contact Active Forms -->
	<script> var _ctct_m = "f9d3a4f62e61c101ded261183c1e2aa4"; </script>
	<script id="signupScript" src="//static.ctctcdn.com/js/signup-form-widget/current/signup-form-widget.min.js" async defer></script>
	<!-- End Constant Contact Active Forms -->
		<header id="site-header">
		<div data-closable class="sl_notification-bar" id="sl_notification-bar">
				<div class="sl_inner">
					<span>Committed to Safety: Latest information on COVID-19 Precautions</span><a href="/important-covid-19-update-from-skywood-recovery/" class="sl_button">Learn More</a>
				</div>
					<button class="close-button" aria-label="Close alert" style="color: #fff;" type="button" data-close>
					    <span aria-hidden="true">&times;</span>
					</button>
				</div>
			</div>
			<div class="row tb-pad-20">
				<div class="medium-12 columns">
					<span id="header-phone" > <?php echo do_shortcode('[frn_phone action="Phone Clicks in Top White Header"]');?> </span>
					<a href="<?php echo site_url(); ?>" id="site-logo-link">
						<img src="<?php echo get_template_directory_uri()  ?>/images/SKY-fullcolor.png" alt="Skywood Recovery" class="header-logo-gray">
						<img src="<?php echo get_template_directory_uri()  ?>/images/SKY-fullcolor-inverse.png" alt="Skywood Recovery" class="header-logo-fc">
					</a>
					<div class="clearfix"></div>
				</div>
			</div>
		</header>
		<div class="clearfix"></div>
		<div id="navStopper">
			<div data-sticky-container>
				<div class="sticky" data-sticky data-sticky-on="small" data-options="marginTop:0;" data-anchor="navStopper" style="width: 100%;">
					<div id="main-nav">
						<div id="large-nav">
							<div class="row">
								<div class="large-12 columns">
									<!-- <ul class="menu logo-menu">
										<li class=""><div><a href="<?php echo site_url(); ?>"><div id="nav-logo" class="hide-logo"></div></a></div></li>
									</ul> -->
									<span id="primary-nav-links">
										<?php wp_bootstrap_main_nav(); // Adjust using Menus in Wordpress Admin ?>
									</span>
									
									<a href="." id="mega-btn" class="" onClick="ga('send', 'event', 'Mega Menu', 'Menu Opens & Closes');" ></a>
									<ul class="menu phone-menu float-right">
										<!-- <li class=""><div><?php echo do_shortcode('[frn_phone action="Phone Clicks in Floating Header"]');?></div></li> -->
										<li class=""><a id="top-nav-search-btn" href="#" class="open-top-search" onClick="ga('send', 'event', 'Search Bar', 'Activate Bar');" ><div id="top-nav-search-icon"></div></a></li>
									</ul>
								</div>
							</div>
						</div>
						<div id="mega-menu">
							<div class="row">
								<div class="large-10 columns large-centered">
									<?php wp_bootstrap_expanded_nav(); // Adjust using Menus in Wordpress Admin ?>
								</div>
							</div>
						</div>
						<div id="top-search">
							<div class="row">
								<div class="large-12 columns">
									<form action="<?php echo home_url( '/' ); ?>" method="get" class="form-inline search-form">
										<div class="input-group">
											<span class="input-group-label show-for-lt-ie9">Search</span>
											<input class="input-group-field form-control" name="s" id="search" placeholder="<?php _e("Search...","wpbootstrap"); ?>" value="" type="text">
											<button type="submit" class="button"><?php _e("Go","wpbootstrap"); ?></button>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div id="global-side-nav" class="expanded">
				<ul class="menu vertical">
					<li><!--<a id="snPhone" class=""></a>--><?php 
						echo do_shortcode('[frn_phone id="snPhone" category="Contact Options Flyout" action="Phone Clicks in Contact Flyout"]');
						$id="snContact";
						if(function_exists('frn_chat_offline')) {
							$offline=frn_chat_offline();
							if($offline=="yes") $id="snChat";
						}
						?></li>
					<li><a href="/contact" id="<?=$id;?>" onClick="ga('send', 'event', 'Contact Options Flyout', 'Contact Page');" >Contact</a></li>
					<li><?php 
						echo do_shortcode('[lhn_inpage text="Chat" offline="Email" id="snChat" offline_id="snContact" category="Contact Options Flyout" action="Chat Button Clicks"]');  
						/* 
							//Commented out by Dax 9/26/17 when reverting to updated plugin version
						if(is_chat_live()):  //went before <li>
							<a href="#" id="snChat" class="chat-launch" onClick="ga('send', 'event', 'Contact Options Flyout', 'Chat/Email');">Chat</a>
						endif; //went after </li>
						*/ 
					?></li>
				</ul>
			</div>
			<div id="content">