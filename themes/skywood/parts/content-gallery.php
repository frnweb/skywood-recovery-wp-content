<?php
if( have_rows('gallery') ) : 
?>
<!-- Carousel for Small -->
<section class="sl_module sl_module--gallery sl_gallery ">
    <div class="sl_inner">
        <div class="sl_gallery__slider">

            <?php
            /* Gallery Loop */
            if( have_rows('gallery') ) : 

                // loop images
                while ( have_rows('gallery') ) : the_row();

                    //Accordion Fields
                    $image = get_sub_field('image');

                    echo '<img class="sl_gallery__slider__slide" alt="'.$image['alt'].'" src="' . $image['url'] . ')" />';

                endwhile;
            endif;
            /* End Gallery Loop */
            ?>
        </div><!--/.sl_gallery__slider-->
    </div><!-- sl_inner -->
</section>
<?php endif; ?>